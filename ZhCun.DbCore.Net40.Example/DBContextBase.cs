﻿/**************************************************************************
创建日期:	2019/5/12 23:13:28     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn

描	述：
记	录：
***************************************************************************/
using ZhCun.DbCore.Cores;

namespace ZhCun.DbCore.Example
{
    class DBContextBase : DBContextDebug
    {
        static readonly string ConnStr = "Server=.;Database=ZhCun_DBCore;Trusted_Connection=True";

        public DBContextBase() : base(EmDbType.SQLServer, ConnStr)
        { }
    }
}
