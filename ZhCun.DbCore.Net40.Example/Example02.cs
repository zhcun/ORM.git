﻿/**************************************************************************
创建日期:	2019/5/12 23:11:46     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ZhCun.DbCore.Example.DbModels;

namespace ZhCun.DbCore.Example
{
    class Example02
    {        
        [QueryNotFilter]
        public static void Query1()
        {
            //查询所有数据
            var db = new DBContext01();
            var r = db.Query<TUser>(s => true);
            var rList = r.ToList();
            Common.PrintList(rList);
        }
        public static void Query2()
        {
            //使用lamad表达式查询
            var db = new DBContext01();
            var r = db.Query<TUser>(s => s.UserName == "张三");
            var rList = r.ToList();
            Common.PrintList(rList);
        }
        public static void Query3()
        {
            //使用query对象查询
            var db = new DBContext01();
            var query = db.CreateQuery<TUser>();
            query.WhereAnd(s => s.WEx_Like(s.UserName, "%张三%"));
            query.WhereOr(s => s.Email != null);
            var r = db.Query<TUser>(query);
            var rList = r.ToList();
            Common.PrintList(rList);
        }
        public static void Query4()
        {
            var db = new DBContext01();
            //使用分页查询
            var query = db.CreateQuery<TUser>();
            query.PageNo = 2;
            query.PageSize = 3;
            query.WhereAnd(s => s.UserName != null).OrderBy(s => s.UserName);
            var r = db.Query<TUser>(query);
            var rList = r.ToList();
            Common.PrintList(rList);
        }
    }
}