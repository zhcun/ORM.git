﻿/*
	功能: 增加用户数据
	描述:				
			
	作者: 张存
	日期: 2019.5.15
	修改记录(注明修改人,修改时间):
		
		
*/
ALTER Proc [dbo].[P_AddUser]
	@LoginName varchar(50),
	@LoginPwd varchar(50),
	@UserName varchar(50),
	@Success bit output, -- 输出执行结果
	@Message varchar(50) output -- 输出执行消息
as
begin
	insert into TUser(Id,LoginName,LoginPwd,UserName)
	select NEWID(),@LoginName,@LoginPwd,@UserName
	
	set @Success = 1;
	set @Message = '新增用户成功';
end	