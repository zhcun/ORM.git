using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.DbCore.Example.DbModels
{
	public partial class TDevice : EntityBase
	{
		private System.Guid _Id;
		/// <summary>
		/// Id
		/// </summary>
		[EntityAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.Guid Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _DevName;
		/// <summary>
		/// DevName
		/// </summary>
		[EntityAttribute(ColumnName = CNDevName)]
		public System.String DevName
		{
			get { return _DevName; }
			set
			{
				_DevName = value;
				base.SetFieldChanged(CNDevName) ;
			}
		}

		private System.Guid? _DevType;
		/// <summary>
		/// DevType
		/// </summary>
		[EntityAttribute(ColumnName = CNDevType)]
		public System.Guid? DevType
		{
			get { return _DevType; }
			set
			{
				_DevType = value;
				base.SetFieldChanged(CNDevType) ;
			}
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[EntityAttribute(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set
			{
				_Remark = value;
				base.SetFieldChanged(CNRemark) ;
			}
		}

		private System.DateTime? _CreateTime;
		/// <summary>
		/// CreateTime
		/// </summary>
		[EntityAttribute(ColumnName = CNCreateTime)]
		public System.DateTime? CreateTime
		{
			get { return _CreateTime; }
			set
			{
				_CreateTime = value;
				base.SetFieldChanged(CNCreateTime) ;
			}
		}

		private System.DateTime? _ModifyTime;
		/// <summary>
		/// ModifyTime
		/// </summary>
		[EntityAttribute(ColumnName = CNModifyTime)]
		public System.DateTime? ModifyTime
		{
			get { return _ModifyTime; }
			set
			{
				_ModifyTime = value;
				base.SetFieldChanged(CNModifyTime) ;
			}
		}

		private System.Guid? _UserId;
		/// <summary>
		/// UserId
		/// </summary>
		[EntityAttribute(ColumnName = CNUserId)]
		public System.Guid? UserId
		{
			get { return _UserId; }
			set
			{
				_UserId = value;
				base.SetFieldChanged(CNUserId) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNDevName = "DevName";
		public const string CNDevType = "DevType";
		public const string CNRemark = "Remark";
		public const string CNCreateTime = "CreateTime";
		public const string CNModifyTime = "ModifyTime";
		public const string CNUserId = "UserId";
		#endregion

	}
}
