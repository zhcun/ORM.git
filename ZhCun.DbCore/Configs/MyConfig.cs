﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.DbCore.Configs;

namespace ZhCun.DbCore
{
    static class MyConfig
    {
        static MyConfig()
        {
            ConfigTools.SetConfigObj(typeof(MyConfig));
            ConfigTools.SetConfigObj(Oracle, nameof(Oracle));
            ConfigTools.SetConfigObj(SqlServer, nameof(SqlServer));
            ConfigTools.SetConfigObj(SQLite, nameof(SQLite));
            ConfigTools.SetConfigObj(MySql, nameof(MySql));
        }

        public static string Test { set; get; } = "this test";

        public static OracleConfig Oracle { get; } = new OracleConfig();

        public static SqlServerConfig SqlServer { get; } = new SqlServerConfig();

        public static SQLiteConfig SQLite { get; } = new SQLiteConfig();

        public static MySqlConfig MySql { get; } = new MySqlConfig();

    }

    class OracleConfig
    {
        /// <summary>
        /// 参数是否以名称指定，而不是顺序, 默认 true
        /// </summary>
        public bool BindByName { set; get; } = true;
        /// <summary>
        /// 是否将表名或者列名用双引号，oracle 加引号会大小写敏感 ,默认 false
        /// </summary>
        public bool IsFormatName { set; get; } = false;
        /// <summary>
        /// 当加引号的列名或表名后，是否强制转换为大写 (默认 true)
        /// </summary>
        public bool IsForceUpper { set; get; } = true;
    }

    class SqlServerConfig
    {

    }

    class SQLiteConfig
    {

    }

    class MySqlConfig
    {

    }
}