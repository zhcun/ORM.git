﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;

namespace ZhCun.DbCore.Configs
{
    class ConfigTools
    {
        const string SectionName = "ZhCun.DBCore";

        public static void SetConfigObj(object configObj,string section = null)
        {
            if (string.IsNullOrWhiteSpace(section))
            {
                section = SectionName;
            }
            else
            {
                section = $"{SectionName}.{section}";
            }
            var configItems = ConfigurationManager.GetSection(section) as NameValueCollection;
            if (configItems == null) return;

            foreach (var key in configItems.AllKeys)
            {
                var v = configItems[key];
                if (v == null) continue;
                Reflection.SetPropertyValue(configObj, key, v);
            }
        }        
    }
}