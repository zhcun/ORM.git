﻿namespace ZhCun.DbCore
{
    public enum EmDbType
    {
        SQLServer = 1,
        SQLite = 2,
        Oracle = 3,
        MySQL = 4
    }
}
