﻿using System.Data.SqlClient;
using System.Data.Common;
using ZhCun.DbCore.BuildSQLText;
using System.Data;
using System;
using ZhCun.DbCore.Exceptions;
using System.Collections.Generic;

namespace ZhCun.DbCore.DBHelper
{
    public class SQLHelper : DbHelperBase
    {
        public SQLHelper(string connStr)
            : base(connStr)
        { }

        SqlConnection _DBConnectionObj;
        SqlCommand _DbCommandObj;
        SqlDataAdapter _DbDataAdapterObj;

        public override DbConnection DBConnectionObj
        {
            get
            {
                //SqlBulkCopy aa = new SqlBulkCopy(new SqlConnection());
                if (_DBConnectionObj == null)
                {
                    _DBConnectionObj = new SqlConnection(ConnStr);
                }
                return _DBConnectionObj;
            }
        }

        public override DbCommand DbCommandObj
        {
            get
            {
                if (_DbCommandObj == null)
                {
                    _DbCommandObj = new SqlCommand();
                }
                return _DbCommandObj;
            }
        }

        protected internal override DbDataAdapter DbDataAdapterObj
        {
            get
            {
                if (_DbDataAdapterObj == null)
                {
                    _DbDataAdapterObj = new SqlDataAdapter();
                }
                return _DbDataAdapterObj;
            }
        }

        //public override DbParameter[] ConvertDbParameter(Dictionary<string, object> paramObj)
        //{
        //    if (paramObj == null) return null;
        //    List<DbParameter> paramList = new List<DbParameter>();
        //    foreach (var item in paramObj.Keys)
        //    {
        //        paramList.Add(new SqlParameter(item, paramObj[item]));
        //    }
        //    return paramList.ToArray();
        //}

        public override DbParameter CreateDbParameter(string paramName, object paramValue)
        {
            if (paramName.Substring(0, 1) != BuildSQLBase.PARAMSIGN_SQLSERVER)
            {
                paramName = string.Format("{1}{0}", paramName, BuildSQLBase.PARAMSIGN_SQLSERVER);
            }
            return new SqlParameter(paramName, paramValue);
        }

        /// <summary>
        /// 批量拷贝插入
        /// </summary>
        public override int BulkInsert(DataTable dt)
        {
            SqlBulkCopy bulkCopy;
            if (IsTrans)
            {
                bulkCopy = new SqlBulkCopy(_DBConnectionObj, SqlBulkCopyOptions.Default, DbTransObj as SqlTransaction);
            }
            else
            {
                bulkCopy = new SqlBulkCopy(_DBConnectionObj);
            }
            try
            {
                bulkCopy.DestinationTableName = dt.TableName;
                bulkCopy.BatchSize = dt.Rows.Count;
                OpenConnection();
                bulkCopy.WriteToServer(dt);
                return dt.Rows.Count;
            }
            catch (Exception ex)
            {
                var dbEx = new DbHelperException("SqlServer BulkInsert 发生异常", this, ex);
                throw dbEx;
            }
            finally
            {
                //这里关闭也能正常走事务处理
                bulkCopy.Close(); 
                CloseConnect();
            }
        }
    }
}