﻿using System.Data.SQLite;
using System.Data.Common;
using ZhCun.DbCore.BuildSQLText;
using System.Data;
using System;

namespace ZhCun.DbCore.DBHelper
{
    public class SQLiteHelper : DbHelperBase
    {
        public SQLiteHelper(string connStr)
            : base(connStr)
        { }

        SQLiteConnection _DBConnectionObj;
        SQLiteCommand _DbCommandObj;
        SQLiteDataAdapter _DbDataAdapterObj;

        public override DbConnection DBConnectionObj
        {
            get
            {
                if (_DBConnectionObj == null)
                {
                    _DBConnectionObj = new SQLiteConnection(ConnStr);
                }
                return _DBConnectionObj;
            }
        }

        public override DbCommand DbCommandObj
        {
            get
            {
                if (_DbCommandObj == null)
                {
                    _DbCommandObj = new SQLiteCommand();
                }
                return _DbCommandObj;
            }
        }

        protected internal override DbDataAdapter DbDataAdapterObj
        {
            get
            {
                if (_DbDataAdapterObj == null)
                {
                    _DbDataAdapterObj = new SQLiteDataAdapter();
                }
                return _DbDataAdapterObj;
            }
        }

        public override DbParameter CreateDbParameter(string paramName, object paramValue)
        {
            if (paramName.Substring(0, 1) != BuildSQLBase.PARAMSIGN_SQLITE)
            {
                paramName = string.Format("{1}{0}", paramName, BuildSQLBase.PARAMSIGN_SQLITE);
            }
            return new SQLiteParameter(paramName, paramValue);
        }
        //#if !NET40
#if !NETSTANDARD
        /// <summary>
        /// 修改sqlite数据库密码
        /// </summary>
        /// <param name="connStr">连接字符串</param>
        /// <param name="newPassword">要设置的新密码</param>
        public static void ChangePassword(string connStr, string newPassword)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connStr))
            {
                try
                {
                    conn.Open();
                    conn.ChangePassword(newPassword);
                    conn.Close();
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
        }
#endif

        /// <summary>
        /// 批量插入
        /// </summary>
        public override int BulkInsert(DataTable dt)
        {
            throw new NotImplementedException("sqlite 不支持批量插入操作，直接使用事务操作");
        }
    }
}