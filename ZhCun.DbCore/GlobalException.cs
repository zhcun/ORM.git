﻿/**************************************************************************
创建日期:	2019/5/13
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)  ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.DbCore.DBHelper;
using ZhCun.DbCore.Exceptions;

namespace ZhCun.DbCore
{
    public class ArgException
    {
        public DbHelperException DBException { set; get; }

        public bool CancelThrow { set; get;  }
    }


    public static class GlobalException
    {
        /// <summary>
        /// 初始DBHelper异常的事件
        /// </summary>
        public static event Action<ArgException> OnContextException;
        /// <summary>
        /// 处理一个全局异常，当返回true时阻止抛出异常
        /// </summary>
        internal static bool ExceptionHandle(string message,Exception ex,IDbHelper dbHelper) 
        {
            if (OnContextException != null)
            {
                ArgException arg = new ArgException
                {
                    DBException = new DbHelperException(message, dbHelper, ex)
                };
                OnContextException(arg);
                return arg.CancelThrow;
            }
            return false;
        }
    }
}
