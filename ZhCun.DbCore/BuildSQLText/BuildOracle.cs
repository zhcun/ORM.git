﻿using System.Text;

namespace ZhCun.DbCore.BuildSQLText
{
    class BuildOracle : BuildSQLBase
    {
        protected internal override string ParamSignStr
        {
            get { return PARAMSIGN_ORACLE; }
        }

        public override string FormatName(string fieldNameOrTableName)
        {
            if (!MyConfig.Oracle.IsFormatName)
            {
                return fieldNameOrTableName;
            }

            if (MyConfig.Oracle.IsForceUpper)
            {
                return $"\"{fieldNameOrTableName.ToUpper()}\"";
            }
            else
            {
                return $"\"{fieldNameOrTableName}\"";
            }
        }

        public override ISqlBuilder BuildQueryPager(string tableName, string[] fields, int pageNo, int onePage)
        {
            StringBuilder sql = new StringBuilder();
            string searchFieldStr = GetSelectFieldStr(fields);
            sql.Append("Select * ");
            sql.Append("FROM (SELECT tt.*, ROWNUM AS rowno ");
            sql.AppendFormat("FROM (  SELECT {0} ", searchFieldStr);
            sql.AppendFormat("FROM {0} t ", tableName);
            sql.Append("Where {0} "); //where预留
            sql.Append("Order By {1}"); //order by 预留
            sql.Append(") tt ");
            sql.AppendFormat("WHERE ROWNUM <= {0}) table_alias ", pageNo * onePage);
            sql.AppendFormat("WHERE table_alias.rowno > {0} ", (pageNo - 1) * onePage);
            ISqlBuilder result = CreateBuildResult();
            result.AddSQLText(sql.ToString());
            return result;
        }
    }
}
