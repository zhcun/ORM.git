﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.DbCore
{
    /// <summary>
    /// 过滤的标记特性,默认为忽略（Ignore = true）
    /// </summary>
    public class QueryNotFilterAttribute : Attribute
    { }
}