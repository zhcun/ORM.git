﻿/*
创建日期:	2019/5/11 23:54:20     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn
描	述：
记	录：
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ZhCun.DbCore.BuildSQLText;
using ZhCun.DbCore.DBHelper;
using ZhCun.DbCore.Entitys;

namespace ZhCun.DbCore.Cores
{
    /// <summary>
    /// sql执行结果对象（执行前）
    /// </summary>
    public class QueryResult<TEntity> : QueryResult where TEntity : EntityBase, new()
    {
        public QueryResult(IDbHelper dbHelper, ISqlBuilder sqlBuilder)
            : base(dbHelper, sqlBuilder)
        { }

        /// <summary>
        /// 返回泛型列表，执行是否首行，是否用不反回null
        /// </summary>
        /// <param name="isFirst">true:返回首行</param>
        /// <param name="notNull">true：如果没有数据也不反回null</param>
        /// <returns></returns>
        protected List<TEntity> ToList(bool isFirst, bool notNull)
        {
            return ToList<TEntity>(isFirst, notNull);
        }

        /// <summary>
        /// 返回实体对象列表
        /// </summary>
        public List<TEntity> ToList()
        {
            return ToList(false);
        }
        /// <summary>
        /// 返回实体对象列表,指定是否返回null
        /// </summary>
        public List<TEntity> ToList(bool notNull)
        {
            return ToList(false, notNull);
        }
        /// <summary>
        /// 返回实体对象
        /// </summary>
        public TEntity ToEntity()
        {
            return ToEntity(false);
        }
        /// <summary>
        /// 返回实体对象,指定是否返回null
        /// </summary>
        public TEntity ToEntity(bool notNull)
        {
            List<TEntity> rList = ToList(true, false);
            if (rList != null && rList.Count > 0)
            {
                return rList[0];
            }
            else
            {
                if (notNull)
                {
                    return new TEntity();
                }
                return null;
            }
        }
        /// <summary>
        /// 返回DataTable对象，并且赋值TableName
        /// </summary>
        public override DataTable ToDataTable()
        {
            DataTable dt = base.ToDataTable();
            if (!string.IsNullOrWhiteSpace(TableName))
            {
                dt.TableName = TableName;
            }
            SetIntervalResult();
            return dt;
        }
    }
}