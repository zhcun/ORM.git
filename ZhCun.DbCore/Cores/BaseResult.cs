﻿/**************************************************************************
创建日期:	2019/5/13 11:19:31     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)  ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ZhCun.DbCore.Cores
{
    /// <summary>
    /// 执行sql结果的
    /// </summary>
    public abstract class BaseResult
    {
        public BaseResult()
        {
            ExecInterval = -1;
        }
        /// <summary>
        /// 开始执行时间
        /// </summary>
        DateTime? _StartTime;

        public string TableName { internal set; get; }

        /// <summary>
        /// 返回执行所影响行数
        /// </summary>
        public int RowCount
        {
            get; protected internal set;
        }
        /// <summary>
        /// 本次操作所执行的间隔（毫秒）
        /// </summary>
        public double ExecInterval { get; private set; }
        /// <summary>
        /// 表示此次执行的结果标记，如：insert,update,query1,query2
        /// </summary>
        public string ResultMark { protected internal set; get; }

        Action<BaseResult> TimingEndHandle;

        protected string _SqlContent;
        /// <summary>
        /// 返回执行sql内容及参数
        /// </summary>
        public abstract string SqlContent { get; }
        /// <summary>
        /// 设置计时器开始时间
        /// </summary>
        protected bool SetTimingHandle(DateTime startTime, Action<BaseResult> argTimingEndHandle)
        {
            if (_StartTime != null || _StartTime < startTime)
            {
                return false;
            }
            TimingEndHandle = argTimingEndHandle;
            _StartTime = startTime;
            return true;
        }
        /// <summary>
        /// 开始计时，并设置开始计时的时间与备注标记
        /// </summary>
        protected internal abstract bool SetTiming(DateTime startTime, Action<BaseResult> argTimingEndHandle);
        /// <summary>
        /// 获取执行间隔，并且赋值给 ExecInterval
        /// </summary>
        protected void SetIntervalResult()
        {
            if (_StartTime != null)
            {
                var timeSpan = DateTime.Now - _StartTime;
                ExecInterval = timeSpan.Value.TotalSeconds;
                _StartTime = null;
            }
            //简化写法，类似与!=null 后 Invoke
            TimingEndHandle?.Invoke(this);
        }
    }
}