﻿/*
创建日期:	2019/5/11 23:54:20     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn
描	述：
记	录：
*/
using System;
using ZhCun.DbCore.BuildSQLText;
using ZhCun.DbCore.DBHelper;

namespace ZhCun.DbCore.Cores
{
    public class ScalarResult : ExecResult
    {
        public ScalarResult(IDbHelper dbHelper, ISqlBuilder sqlBuilder)
            : base(sqlBuilder)
        {
            this._DbHelper = dbHelper;
        }

        IDbHelper _DbHelper;

        public object ToObject()
        {
            var dbParam = _DbHelper.ConvertDbParameter(SqlResult.DbParam);
            object obj = _DbHelper.GetScalar(SqlResult.SQLText, dbParam);
            RowCount = (obj == null || obj is DBNull) ? 0 : 1;
            SetIntervalResult();
            return obj;
        }

        public T ToObject<T>()
        {
            object obj = ToObject();
            if (obj == null || obj is DBNull)
            {
                return default;
            }
            object result = Convert.ChangeType(obj, typeof(T));
            SetIntervalResult();
            return (T)result;
        }
        protected internal override bool SetTiming(DateTime startTime, Action<BaseResult> argTimingEndHandle)
        {
            return SetTimingHandle(startTime, argTimingEndHandle);
        }
    }
}