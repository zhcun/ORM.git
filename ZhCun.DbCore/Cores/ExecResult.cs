﻿/*
创建日期:	2019/5/11 23:54:20     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn
描	述：  增、删、改、的结果对象
记	录：
*/
using System;
using System.Diagnostics;
using System.Text;
using ZhCun.DbCore.BuildSQLText;

namespace ZhCun.DbCore.Cores
{
    /// <summary>
    /// 增、删、改、的结果对象
    /// </summary>
    public class ExecResult : BaseResult
    {
        protected internal ExecResult(ISqlBuilder sqlBuilder)
            : this(0, sqlBuilder)
        { }
        protected internal ExecResult(int execCount, ISqlBuilder sqlBuilder)
        {
            this.RowCount = execCount;
            this.SqlResult = sqlBuilder;
        }
        /// <summary>
        /// sql建造器
        /// </summary>
        protected internal ISqlBuilder SqlResult { get; }
        /// <summary>
        /// 返回sql内容
        /// </summary>
        public override string SqlContent
        {
            get
            {
                if (SqlResult == null) return string.Empty;

                if (_SqlContent == null)
                {
                    StringBuilder sql = new StringBuilder();
                    if (SqlResult.DbParam != null)
                    {
                        foreach (var item in SqlResult.DbParam.Keys)
                        {
                            var val = SqlResult.DbParam[item];
                            if (val is DateTime dt)
                            {
                                sql.AppendLine($"declare {item} varchar(100)='{dt:yyyy-MM-dd HH:mm:ss}';");
                            }
                            else if (val is bool bl)
                            {
                                sql.AppendLine($"declare {item} decimal={(bl ? 1 : 0)};");
                            }
                            else if (val is ValueType)
                            {
                                sql.AppendLine($"declare {item} decimal={val};");
                            }
                            else
                            {
                                sql.AppendLine($"declare {item} varchar(100)='{val}';");
                            }
                        }
                    }
                    sql.AppendLine();
                    sql.Append(SqlResult.SQLText);
                    _SqlContent = sql.ToString();
                }
                return _SqlContent;
            }
        }
        /// <summary>
        /// 设置计时器开始时间
        /// </summary>
        protected internal override bool SetTiming(DateTime startTime, Action<BaseResult> argTimingEndHandle)
        {
            if (SetTimingHandle(startTime, argTimingEndHandle))
            {
                SetIntervalResult();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}