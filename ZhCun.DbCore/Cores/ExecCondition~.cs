﻿/*
创建日期:	2019/5/11 23:54:20     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn
描	述：
记	录：
*/
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ZhCun.DbCore.Entitys;

namespace ZhCun.DbCore.Cores
{
    public interface ICondition : IDisposable
    {
        Dictionary<Expression, EmWhereJoin> WhereExpressions { get; }
    }

    /// <summary>
    /// 用于框架的查询条件对象基类，仅包含where条件
    /// </summary>
    /// <typeparam name="TEntity">框架实体类型</typeparam>
    public class ExecCondition<TEntity> : ICondition where TEntity : EntityBase, new()
    {
        public ExecCondition()
        {
            WhereExpressions = new Dictionary<Expression, EmWhereJoin>();
        }

        /// <summary>
        /// where 表达式集合
        /// </summary>
        public Dictionary<Expression, EmWhereJoin> WhereExpressions { get; }
        /// <summary>
        /// 查询条件进行and连接
        /// </summary>
        /// <param name="whereExpress">表达式</param>
        /// <returns>返回当前查询对象</returns>
        public virtual ExecCondition<TEntity> WhereAnd(Expression<Func<TEntity, bool>> whereExpress)
        {
            WhereExpressions.Add(whereExpress, EmWhereJoin.And);
            return this;
        }
        /// <summary>
        /// 查询条件进行or连接
        /// </summary>
        /// <param name="whereExpress">表达式</param>
        /// <returns>返回当前查询对象</returns>
        public virtual ExecCondition<TEntity> WhereOr(Expression<Func<TEntity, bool>> whereExpress)
        {
            WhereExpressions.Add(whereExpress, EmWhereJoin.Or);
            return this;
        }
        /// <summary>
        /// 清除所有已添加的表达式内容
        /// </summary>
        public void ClearExpression()
        {
            WhereExpressions.Clear();
        }
        public void Dispose()
        {
            ClearExpression();
        }
    }
}