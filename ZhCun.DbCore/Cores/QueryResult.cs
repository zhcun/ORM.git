﻿/*
创建日期:	2019/5/11 23:54:20     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn
描	述：
记	录：
    2022.1.21 sql查询，也可以返回泛型列表的实现
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using ZhCun.DbCore.BuildSQLText;
using ZhCun.DbCore.DBHelper;

namespace ZhCun.DbCore.Cores
{
    public class QueryResult : ExecResult
    {
        public QueryResult(IDbHelper dbHelper, ISqlBuilder sqlBuilder)
            : base(sqlBuilder)
        {
            DbHelper = dbHelper;
        }

        protected IDbHelper DbHelper { get; }

        public virtual DataTable ToDataTable()
        {
            DbParameter[] param = DbHelper.ConvertDbParameter(SqlResult.DbParam);
            DataTable dt = DbHelper.GetDataTable(SqlResult.SQLText, param);
            base.RowCount = dt != null ? dt.Rows.Count : 0;
            SetIntervalResult();
            return dt;
        }
        /// <summary>
        /// 返回泛型列表，执行是否首行，是否用不反回null
        /// </summary>
        /// <param name="isFirst">true:返回首行</param>
        /// <param name="notNull">true：如果没有数据也不反回null（0数量列表）</param>
        public List<TEntity> ToList<TEntity>(bool isFirst, bool notNull) where TEntity : class, new()
        {
            DbParameter[] param = DbHelper.ConvertDbParameter(SqlResult.DbParam);
            DbDataReader reader = DbHelper.GetDataReader(SqlResult.SQLText, param);

            List<TEntity> rList = Reflection.GetListByDataReader<TEntity>(reader, isFirst);
            if (notNull && rList == null)
            {
                rList = new List<TEntity>();
            }
            base.RowCount = rList?.Count ?? 0;
            SetIntervalResult();
            return rList;
        }
        /// <summary>
        /// 返回List
        /// </summary>
        public List<TEntity> ToList<TEntity>() where TEntity : class, new()
        {
            return ToList<TEntity>(false, false);
        }
        /// <summary>
        /// 返回首行
        /// </summary>
        public TEntity ToEntity<TEntity>() where TEntity : class, new()
        {
            var rList = ToList<TEntity>(true, false);
            return rList?.FirstOrDefault();
        }
        /// <summary>
        /// 执行查询，返回首行首列
        /// </summary>
        public object ToScalar()
        {
            DbParameter[] param = DbHelper.ConvertDbParameter(SqlResult.DbParam);
            var rObj = DbHelper.GetScalar(SqlResult.SQLText, param);
            SetIntervalResult();
            return rObj;
        }

        /// <summary>
        /// 执行查询，返回首行首列
        /// </summary>
        public T ToScalar<T>()
        {
            var rObj = ToScalar();
            return (T)rObj;
        }
        /// <summary>
        /// 执行查询，判断记录是否存在
        /// </summary>
        public bool ToExists()
        {
            DbParameter[] param = DbHelper.ConvertDbParameter(SqlResult.DbParam);
            var rObj = DbHelper.ExecNonQuery(SqlResult.SQLText, param);
            SetIntervalResult();
            return rObj > 0;
        }

        protected internal override bool SetTiming(DateTime startTime, Action<BaseResult> argTimingEndHandle)
        {
            return SetTimingHandle(startTime, argTimingEndHandle);
        }
    }
}