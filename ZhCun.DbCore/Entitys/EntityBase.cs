﻿/*
创建日期: 2015.8.18
创建者:张存
邮箱:zhangcunliang@126.com
说明:
    实体对象的基类
修改记录: 
    2015.11.8   增加扩展方法 WEx_GreaterThen,WEx_GreaterThenEqual,WEx_LessThan,WEx_LessThanEqual
                实现字符串>=,>,<=,<的表达式解析
    2015.11.9   将扩展方法的fieldName 都改为object,支持直接表达式的类似 "s.Name"输入
    2019.3.5    增加 IsChangeField() 方法，判断属性是否发生变化
    2019.7.23   清除指定字段未改变状态，改为 params
                增加Tag属性，用于实体类里储存扩展对象
    2020.11.14  增加了 OnPropertyChanged 事件
 
 */
using System.Collections.Generic;
using System.Linq;

namespace ZhCun.DbCore.Entitys
{
    /// <summary>
    /// 实体对象基类
    /// </summary>
    public abstract partial class EntityBase
    {
        public EntityBase()
        {
            _ChangedFields = new List<string>();
        }

        readonly List<string> _ChangedFields;
        /// <summary>
        /// 获取所有set过的字属性名
        /// </summary>
        public string[] GetChangedFields()
        {
            return _ChangedFields.ToArray();
        }
        ///// <summary>
        ///// 获取所有改变值的字段数量
        ///// </summary>
        //public int GetChangedFieldLength()
        //{
        //    return _ChangedFields?.Count ?? 0;
        //}
        /// <summary>
        /// 是否有修改的字段值
        /// </summary>
        public bool HasChangedField()
        {
            return _ChangedFields?.Count > 0;
        }
        /// <summary>
        /// 获取属性名称及属性值
        /// </summary>
        internal protected Dictionary<string, object> GetFieldNameValue()
        {
            var r = Reflection.GetPropertyNameAndValue(this);
            return r;
        }
        /// <summary>
        /// 当属性值发生变化(set)时触发 （赋值前）,当返回false 则不进行赋值
        /// </summary>
        protected virtual bool OnPropertyChanged(string field, object oldVal, object newVal)
        {
            return true;
        }

        #region 扩展 比较 表达式

        /// <summary>
        /// int 
        /// </summary>
        public bool WEx_In<T>(object fieldName, params T[] array)
        {
            return true;
        }
        /// <summary>
        /// int not 
        /// </summary>
        public bool WEx_InNot<T>(object fieldName, params T[] array)
        {
            return true;
        }
        /// <summary>
        /// like 
        /// </summary>
        public bool WEx_Like(object fieldName, string likeStr)
        {
            return true;
        }
        /// <summary>
        /// like not 
        /// </summary>
        public bool WEx_LikeNot(object fieldName, string likeStr)
        {
            return true;
        }
        /// <summary>
        /// 大于
        /// </summary>
        public bool WEx_GreaterThen(object fieldName, string value)
        {
            return true;    //这个是用于字符串比较>
        }
        /// <summary>
        /// 大于等于
        /// </summary>
        public bool WEx_GreaterThenEqual(object fieldName, string value)
        {
            return true;
        }
        /// <summary>
        /// 小于
        /// </summary>
        public bool WEx_LessThan(object fieldName, string value)
        {
            return true;
        }
        /// <summary>
        /// 小于等于
        /// </summary>
        public bool WEx_LessThanEqual(object fieldName, string value)
        {
            return true;
        }

        #endregion

        /// <summary>
        /// 设置指定属性值已改变，它将可能进行新增或更新字段
        /// </summary>
        public virtual void SetFieldChanged(params string[] fieldNames)
        {
            foreach (var item in fieldNames)
            {
                if (!_ChangedFields.Contains(item))
                {
                    _ChangedFields.Add(item);
                }
            }
        }
        /// <summary>
        /// 所有字段都设置为改变状态
        /// </summary>
        public virtual void SetAllFieldChanged()
        {
            var pNames = Reflection.GetPropertyNames(GetType());
            SetFieldChanged(pNames);
        }

        /// <summary>
        /// 设置所有字段值未改变状态，所有字段将不进行新增或更新
        /// </summary>
        public virtual void ClearChangedState()
        {
            _ChangedFields.Clear();
        }
        /// <summary>
        /// 清除指定字段的改变状态（改为未改变）
        /// </summary>
        public virtual void ClearChangedState(params string[] fieldName)
        {
            foreach (var item in fieldName)
            {
                if (IsChangeField(item))
                {
                    _ChangedFields.Remove(item);
                }
            }
        }
        /// <summary>
        /// 判断指定字段是否发生变化（赋值）
        /// </summary>
        public bool IsChangeField(params string[] fieldNames)
        {
            foreach (var fieldName in fieldNames)
            {
                if (_ChangedFields.Exists(s => s == fieldName))
                {
                    return true;
                }
            }
            return false;
        }
    }
}