﻿namespace ZhCun.DbCore.Entitys
{
    public class EntityInfo
    {
        public string PropertyName { set; get; }

        public EntityAttribute PropertyAttribute { set; get; }

    }

    interface IEntityAnaly
    {
        /// <summary>
        /// 获取对象的属性值
        /// </summary>
        object GetPropertyValue<TEntity>(TEntity entity, string propertyName) where TEntity : EntityBase, new();

        string[] GetAllFields<TEntity>() where TEntity : EntityBase, new();

        EntityAttribute GetAttribute<TEntity>(string propertyName) where TEntity : EntityBase, new();

        string GetTableName<TEntity>() where TEntity : EntityBase, new();

        string[] GetPrimaryKey<TEntity>() where TEntity : EntityBase, new();
    }
}