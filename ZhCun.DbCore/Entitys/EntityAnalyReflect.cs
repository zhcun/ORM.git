﻿/*
 
    2016.4.28   增加了实体对象中IsNotField属性,如果为true则表示不是一个数据库字段,字段相关信息会排除
 */

using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace ZhCun.DbCore.Entitys
{
    class EntityAnalyReflect : EntityAnaly
    {
        public EntityAnalyReflect() { }
        /// <summary>
        /// 获取表名
        /// </summary>
        public override string GetTableNameMy<TEntity>()
        {
            var classAttr = Reflection.GetClassAttribute<EntiryClassAttribute, TEntity>();
            if (classAttr != null)
            {
                return classAttr.TableName;
            }
            else
            {
                return typeof(TEntity).Name;
            }
        }
        /// <summary>
        /// 通过解析获得Model的对象的参数,Key:为类的属性名
        /// 如果指定了 IsNotField 不返回
        /// </summary>
        /// <returns>返回model参数</returns>
        protected override List<EntityAttribute> GetAttribute<TEntity>()
        {
            PropertyInfo[] pros = Reflection.GetPropertyInfo<TEntity>();
            List<EntityAttribute> attrList = new List<EntityAttribute>();
            foreach (PropertyInfo item in pros)
            {
                var attr = Reflection.GetCustomAttribute<EntityAttribute>(item);
                if (attr == null)
                {
                    //如果实体没定义属性则创建一个新的
                    attr = new EntityAttribute();
                    attr.ColumnName = item.Name;
                }
                else
                {
                    if (attr.IsNotField) continue;
                    //如果列名没有赋值,则将列名定义和属性名一样的值
                    if (string.IsNullOrEmpty(attr.ColumnName))
                    {
                        attr.ColumnName = item.Name;
                    }
                }
                attrList.Add(attr);
            }
            if (attrList.Exists(s => s.ColumnIndex > 0))
            {
                var orderByList = attrList.OrderBy(s => s.ColumnIndex).ToList();
                return orderByList;
            }
            return attrList;
        }
    }
}