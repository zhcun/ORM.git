﻿/**************************************************************************
创建日期:	2019/5/14 22:44:01     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System.Data;

namespace ZhCun.DbCore.Entitys
{
    /// <summary>
    /// 存储过程“执行”相关的基类
    /// </summary>
    public class ProcEntityExecBase : ProcEntityBase
    {
        [ProcParam(ParamDirection = ParameterDirection.Output)]
        public bool Success { protected internal set; get; }
        /// <summary>
        /// 输出执行结果的信息内容，OutRetValue != 8888 时有效
        /// </summary>
        [ProcParam(ParamDirection = ParameterDirection.Output)]
        public string Message { set; get; }
    }
}