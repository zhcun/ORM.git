﻿/**************************************************************************
创建日期:	2019/5/14  
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn

描	述：存储过程参数的特性
记	录：
***************************************************************************/
using System;
using System.Data;

namespace ZhCun.DbCore.Entitys
{
    /// <summary>
    /// 存储过程参数的特性类
    /// </summary>
    public class ProcParamAttribute : Attribute
    {
        public ProcParamAttribute()
        {
            _ParamDirection = ParameterDirection.Input;
            _OutSize =50;
        }
        public ProcParamAttribute(ParameterDirection paramDirection)
        {
            _ParamDirection = paramDirection;
        }

        ParameterDirection _ParamDirection;
        /// <summary>
        /// 存储过程输出参数的方向类型
        /// </summary>
        public ParameterDirection ParamDirection
        {
            get { return _ParamDirection; }
            set { _ParamDirection = value; }
        }
        int _OutSize;
        /// <summary>
        /// 输出参数的字节数,默认50个字节
        /// </summary>
        public int OutSize
        {
            get { return _OutSize; }
            set { _OutSize = value; }
        }
        /// <summary>
        /// 是否oracle游标类型
        /// </summary>
        public bool IsOracleCursor { set; get; }
        //public string ProcedureName { set; get; }
        /// <summary>
        /// 参数名，空则使用属性名代表参数名
        /// </summary>
        public string ParameterName { set; get; }
        /// <summary>
        /// true表示非存储过程参数
        /// </summary>
        public bool NotParameter { set; get; }
    }
}
