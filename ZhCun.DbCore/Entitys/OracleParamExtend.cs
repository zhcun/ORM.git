/*
时   间:  2015.8.18
作   者:  张存
邮   箱:  zhangcunliang@126.com
描   述:
    
修改记录: 
    2020.2.22  去掉oracle的引用，不使用oracle数据库时可以不需要 Oracle.ManagedDataAccess.dll
 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;

namespace ZhCun.DbCore.Entitys
{
    /// <summary>
    /// 存储过程实体基类
    /// </summary>
    static class OracleParamExtend
    {
        public static void SetOracleCursor(this ProcEntityBase entity, DbParameter dbParam)
        {
            //oracle 返回datatable必须指定一个游标类型的参数用来输出
            OracleParameter param = dbParam as OracleParameter;
            param.OracleDbType = OracleDbType.RefCursor;

            //Type oracleParamType = dbParam.GetType();
            //var oclDbType = oracleParamType.GetProperty("OracleDbType");
            //oclDbType.SetValue(dbParam, Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor, null);
        }
    }
}