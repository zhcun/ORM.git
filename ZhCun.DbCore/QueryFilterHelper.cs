﻿/**************************************************************************
创建日期:	2019/5/12 23:48:19     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn

描	述：  查询过滤器工具类
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.DbCore
{
    public class QueryFilterHelper
    {
        /// <summary>
        /// 查询当前执行的方法是过滤权限
        /// </summary>
        public static bool IsFilter()
        {
            var notFilter = Reflection.GetMethodAttribute<QueryNotFilterAttribute>();
            return notFilter == null;
        }
        /// <summary>
        /// 指定类是否具有
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <returns></returns>
        public static bool IsFilter<TClass>()
        {
            var notFilter = Reflection.GetClassAttribute<QueryNotFilterAttribute, TClass>();
            return notFilter == null;
        }
    }
}