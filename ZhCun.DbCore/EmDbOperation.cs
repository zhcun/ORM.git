﻿namespace ZhCun.DbCore
{
    /// <summary>
    /// 数据库操作类型
    /// </summary>
    public enum EmDbOperation
    {
        /// <summary>
        /// 增加操作
        /// </summary>
        Create,
        /// <summary>
        /// 查询操作
        /// </summary>
        Read,
        /// <summary>
        /// 更新操作
        /// </summary>
        Update,
        /// <summary>
        /// 删除操作
        /// </summary>
        Delete
    }
}