﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.DbCore
{
    /// <summary>
    /// 全局配置
    /// </summary>
    public class GlobalConfig
    {
        /// <summary>
        /// 是否开启数据过滤
        /// </summary>
        public static bool EnableQueryFilter { set; get; }
    }
}