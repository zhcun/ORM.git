﻿using System;
using System.Linq.Expressions;

namespace ZhCun.DbCore.ExpressionAnaly
{
    public interface IExpressionAnaly
    {
        void AnalyWhereExpression<T>(Expression<Func<T, bool>> whereExpr, Func<string, string> formatFieldNameFun, Func<object, string> addParamValueFun);
        void AnalySelectorExpression<TEntity>(Expression<Func<TEntity, object>> selector, Func<string, string> formatFieldNameFun);
        string GetValue();
    }
}
