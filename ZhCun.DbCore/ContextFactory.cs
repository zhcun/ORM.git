﻿using System;
using ZhCun.DbCore.DBHelper;
using ZhCun.DbCore.BuildSQLText;
using ZhCun.DbCore.Entitys;
using ZhCun.DbCore.ExpressionAnaly;

namespace ZhCun.DbCore
{
    class ContextFactory
    {
        static object FactoryLocker = new object();
        static ContextFactory _Instance;
        public static ContextFactory Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (FactoryLocker)
                    {
                        if (_Instance == null) _Instance = new ContextFactory();
                    }
                }
                return _Instance;
            }
        }

        public IDbHelper CreateDbHelper(EmDbType dbType, string connStr)
        {
            IDbHelper dbHelper;
            switch (dbType)
            {
                case EmDbType.SQLServer:
                    dbHelper = new SQLHelper(connStr);
                    break;
                case EmDbType.SQLite:
                    dbHelper = new SQLiteHelper(connStr);
                    break;
                case EmDbType.Oracle:
                    dbHelper = new OracleHelper(connStr);
                    break;
                case EmDbType.MySQL:
                    dbHelper = new MySqlDbHelper(connStr);
                    break;                
                default:
                    throw new Exception("创建dbHelper失败,未支持的数据库类型");
            }
            return dbHelper;
        }
        public BuildSQLBase CreateBuildSQL(EmDbType dbType)
        {
            BuildSQLBase buildSql = null;
            switch (dbType)
            {
                case EmDbType.SQLServer:
                    buildSql = new BuildSQLServer();
                    break;
                case EmDbType.SQLite:
                    buildSql = new BuildSQLite();
                    break;
                case EmDbType.Oracle:
                    buildSql = new BuildOracle();
                    break;
                case EmDbType.MySQL:
                    buildSql = new BuildMySql();
                    break;
                default:
                    throw new Exception("创建BuildSQL失败,未支持的数据库类型");
            }
            return buildSql;
        }
        public IEntityAnaly CreateEntityAnaly()
        {
            //TODO:如果实体解析需要其它（非反射）实现，从这里进行创建
            return new EntityAnalyReflect();
        }
        public IExpressionAnaly CreateExpressionAnaly()
        {
            //TODO:如果表达式有更好的实现，则可替换或扩展
            return new ExprAnalyBase();
        }
    }
}