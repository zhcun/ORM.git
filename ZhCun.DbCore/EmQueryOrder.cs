﻿namespace ZhCun.DbCore
{
    public enum EmQueryOrder
    {
        /// <summary>
        /// 升序
        /// </summary>
        Asc,
        /// <summary>
        /// 降序
        /// </summary>
        Desc
    }
}
