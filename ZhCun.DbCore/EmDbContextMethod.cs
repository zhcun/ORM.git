﻿/**************************************************************************
创建日期:	2019/5/13 10:26:51     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)  ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.DbCore
{
    public enum EmDbContextMethod
    {
        ExecSql,
        Insert,
        InsertBulk,
        Update,
        UpdateLamda,
        UpdateCondition,
        Delete,
        DeleteLamda,
        DeleteCondition,
        QuerySql,
        QueryWhereSql,
        QueryLamda,
        QueryCondition,
        QueryConditionAndSql,
        QueryScalarSql,
        QueryMaxLamda,
        QueryMaxCondition,
        QueryMinLamda,
        QueryMinCondition,
        QuerySumLamda,
        QuerySumCondition,
        QueryCountLamda,
        QueryCountCondition,
        ExecProcedure
    }
}