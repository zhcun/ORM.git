# ORM

#### Description
基于 .Net Framework4.0 与 .NET Standard 2.0 编写的 “蝇量级” ORM框架，实现基本常用功能；
没有太复杂的设计，数据库查询接近于ADO操作；
开发工具：VS 2019
开发框架：.Net 4.0  & .NET Standard 2.0
支持Lamda，复杂对象查询
新增与更新：支持自动识别赋值字段
支持数据过滤
支持各操作的方法扩展与重载
支持查看执行详情（SQL、耗时 等）
支持实体类扩展成员

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)