﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZhCun.SqliteTools
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "数据库文件|*.db|所有文件|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtFileName.Text = ofd.FileName;                
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string connStr = $"Data Source={txtFileName.Text};password={txtSrcPwd.Text};";

            using (SQLiteConnection conn = new SQLiteConnection(connStr))
            {
                try
                {
                    conn.Open();
                    conn.ChangePassword(txtNewPwd.Text);
                    conn.Close();
                    MessageBox.Show("新密码设置成功");

                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }
    }
}