﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SqlServerRestore
{
    class RestoreModel
    {
        public string NewDBName { get; set; }

        string _SaveFullPath;

        public string SaveFullPath
        {
            get
            {
                if (_SaveFullPath.Substring(_SaveFullPath.Length - 1) != "\\")
                {
                    _SaveFullPath += "\\";
                }
                return _SaveFullPath;
            }
            set { _SaveFullPath = value; }
        }


        public string BackupFile { get; set; }
    }
}