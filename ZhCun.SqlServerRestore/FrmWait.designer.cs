﻿namespace ZhCun.SqlServerRestore
{
    partial class FrmWait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbShowInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbShowInfo
            // 
            this.lbShowInfo.AutoSize = true;
            this.lbShowInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbShowInfo.ForeColor = System.Drawing.Color.Red;
            this.lbShowInfo.Location = new System.Drawing.Point(50, 21);
            this.lbShowInfo.Name = "lbShowInfo";
            this.lbShowInfo.Size = new System.Drawing.Size(172, 25);
            this.lbShowInfo.TabIndex = 0;
            this.lbShowInfo.Text = "正在执行,请稍后...";
            // 
            // FrmWait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 60);
            this.Controls.Add(this.lbShowInfo);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmWait";
            this.Opacity = 0.5D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmWait";
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.FrmWait_MouseDoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbShowInfo;
    }
}