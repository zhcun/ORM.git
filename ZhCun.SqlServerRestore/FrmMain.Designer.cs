﻿
namespace ZhCun.SqlServerRestore
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.lblStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTestConn = new System.Windows.Forms.Button();
            this.txtUpwd = new System.Windows.Forms.TextBox();
            this.txtUId = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnOpenFile = new System.Windows.Forms.Button();
            this.btnSavePath = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNewDbName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.BtnExec = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBakFile = new System.Windows.Forms.TextBox();
            this.btnAbort = new System.Windows.Forms.Button();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(120, 20);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(522, 29);
            this.lblStatus.TabIndex = 35;
            this.lblStatus.Text = "测试连接后,点击[执行]开始恢复数据库";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTestConn);
            this.groupBox1.Controls.Add(this.txtUpwd);
            this.groupBox1.Controls.Add(this.txtUId);
            this.groupBox1.Controls.Add(this.txtServer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(272, 209);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "连接数据库参数";
            // 
            // btnTestConn
            // 
            this.btnTestConn.Location = new System.Drawing.Point(82, 148);
            this.btnTestConn.Name = "btnTestConn";
            this.btnTestConn.Size = new System.Drawing.Size(159, 33);
            this.btnTestConn.TabIndex = 8;
            this.btnTestConn.Text = "测试连接";
            this.btnTestConn.UseVisualStyleBackColor = true;
            this.btnTestConn.Click += new System.EventHandler(this.btnTestConn_Click);
            // 
            // txtUpwd
            // 
            this.txtUpwd.Location = new System.Drawing.Point(82, 93);
            this.txtUpwd.Name = "txtUpwd";
            this.txtUpwd.PasswordChar = '*';
            this.txtUpwd.Size = new System.Drawing.Size(159, 26);
            this.txtUpwd.TabIndex = 7;
            this.txtUpwd.TextChanged += new System.EventHandler(this.txtServer_TextChanged);
            // 
            // txtUId
            // 
            this.txtUId.Location = new System.Drawing.Point(82, 57);
            this.txtUId.Name = "txtUId";
            this.txtUId.Size = new System.Drawing.Size(159, 26);
            this.txtUId.TabIndex = 6;
            this.txtUId.Text = "sa";
            this.txtUId.TextChanged += new System.EventHandler(this.txtServer_TextChanged);
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(82, 23);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(159, 26);
            this.txtServer.TabIndex = 5;
            this.txtServer.Text = ".";
            this.txtServer.TextChanged += new System.EventHandler(this.txtServer_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "密码：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "用户名：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务器：";
            // 
            // BtnOpenFile
            // 
            this.BtnOpenFile.Location = new System.Drawing.Point(608, 152);
            this.BtnOpenFile.Name = "BtnOpenFile";
            this.BtnOpenFile.Size = new System.Drawing.Size(55, 29);
            this.BtnOpenFile.TabIndex = 44;
            this.BtnOpenFile.Text = "选择";
            this.BtnOpenFile.UseVisualStyleBackColor = true;
            this.BtnOpenFile.Click += new System.EventHandler(this.BtnOpenFile_Click);
            // 
            // btnSavePath
            // 
            this.btnSavePath.Location = new System.Drawing.Point(610, 97);
            this.btnSavePath.Name = "btnSavePath";
            this.btnSavePath.Size = new System.Drawing.Size(55, 29);
            this.btnSavePath.TabIndex = 43;
            this.btnSavePath.Text = "选择";
            this.btnSavePath.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSavePath.UseVisualStyleBackColor = true;
            this.btnSavePath.Click += new System.EventHandler(this.btnSavePath_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(301, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 20);
            this.label5.TabIndex = 38;
            this.label5.Text = "数据保存路径：";
            // 
            // txtNewDbName
            // 
            this.txtNewDbName.Location = new System.Drawing.Point(396, 190);
            this.txtNewDbName.Name = "txtNewDbName";
            this.txtNewDbName.Size = new System.Drawing.Size(206, 26);
            this.txtNewDbName.TabIndex = 46;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(310, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 20);
            this.label6.TabIndex = 45;
            this.label6.Text = "数据库名称：";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(445, 237);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 33);
            this.btnClose.TabIndex = 42;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // BtnExec
            // 
            this.BtnExec.Location = new System.Drawing.Point(333, 237);
            this.BtnExec.Name = "BtnExec";
            this.BtnExec.Size = new System.Drawing.Size(75, 33);
            this.BtnExec.TabIndex = 41;
            this.BtnExec.Text = "执行";
            this.BtnExec.UseVisualStyleBackColor = true;
            this.BtnExec.Click += new System.EventHandler(this.BtnExec_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(306, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 37;
            this.label4.Text = "备份文件：";
            // 
            // txtBakFile
            // 
            this.txtBakFile.AllowDrop = true;
            this.txtBakFile.Location = new System.Drawing.Point(305, 155);
            this.txtBakFile.Name = "txtBakFile";
            this.txtBakFile.Size = new System.Drawing.Size(297, 26);
            this.txtBakFile.TabIndex = 40;
            this.txtBakFile.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtBakFile_DragDrop);
            this.txtBakFile.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtBakFile_DragEnter);
            // 
            // btnAbort
            // 
            this.btnAbort.Location = new System.Drawing.Point(547, 237);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(75, 33);
            this.btnAbort.TabIndex = 47;
            this.btnAbort.Text = "帮助";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(305, 99);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.Size = new System.Drawing.Size(299, 26);
            this.txtSavePath.TabIndex = 39;
            this.txtSavePath.Text = "d:\\SqlData";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(692, 297);
            this.Controls.Add(this.BtnOpenFile);
            this.Controls.Add(this.btnSavePath);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNewDbName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.BtnExec);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBakFile);
            this.Controls.Add(this.btnAbort);
            this.Controls.Add(this.txtSavePath);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblStatus);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SqlServer 还原备份文件工具";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnTestConn;
        private System.Windows.Forms.TextBox txtUpwd;
        private System.Windows.Forms.TextBox txtUId;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnOpenFile;
        private System.Windows.Forms.Button btnSavePath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNewDbName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button BtnExec;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBakFile;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.TextBox txtSavePath;
    }
}

