﻿namespace ZhCun.CodeBuilder.Builders
{
    public class BuilderFactory
    {
        public static BaseBuilder CreateSqlServerBilder(string connStr)
        {
            return new SqlServerBuilder(connStr);
        }
        public static BaseBuilder CreateSQLiteBilder(string connStr)
        {
            return new SQLiteBuilder(connStr);
        }
        public static BaseBuilder CreateMySql(string connStr)
        {
            return new MySqlBuilder(connStr);
        }
        public static BaseBuilder CreateOleDb(string connStr)
        {
            return new OleDbBuiler(connStr);
        }
        public static BaseBuilder CreateOracle(string connStr)
        {
            return new OracleBuiler(connStr);
        }
    }
}