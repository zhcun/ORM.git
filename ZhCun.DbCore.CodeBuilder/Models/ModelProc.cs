﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.CodeBuilder
{
    public class ModelProc
    {
        /// <summary>
        /// 存储过程名
        /// </summary>
        public string ProcName { set; get; }
        /// <summary>
        /// 存储过程类型,过程还是函数,取值 FUNCTION,PROCEDURE    
        /// </summary>
        public string ProcType { set; get; }
        /// <summary>
        /// 存储过程说明
        /// </summary>
        public string ProcRemark { set; get; }
    }
}
