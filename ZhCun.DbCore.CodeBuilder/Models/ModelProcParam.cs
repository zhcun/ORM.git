﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.CodeBuilder
{
    public class ModelProcParam
    {
        public string ParamName { set; get; }
        /// <summary>
        /// 参数输出类型,In,InOut
        /// </summary>
        public string ParamOutType { set; get; }
        /// <summary>
        /// 参数数据类型,SQLServer
        /// </summary>
        public string ParamDataType { set; get; }
        /// <summary>
        /// c#的数据类型
        /// </summary>
        public string ParamDataType4CSharp { set; get; }
        /// <summary>
        /// 参数长度,如varchar(50)则返回50
        /// </summary>
        public int ParamLength { set; get; }
        /// <summary>
        /// 存储过程参数说明
        /// </summary>
        public string ParamRemark { set; get; }
    }
}
