﻿namespace ZhCun.CodeBuilder
{
    /// <summary>
    /// 此类定义了系统的列信息(非数据库表映射实体)
    /// </summary>
    public class ModelField
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 列名称
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// 列说明
        /// </summary>
        public string ColumnRemark { get; set; }

        /// <summary>
        /// 列顺序序号
        /// </summary>
        public int ColumnOrder { get; set; }

        /// <summary>
        /// 是否自动增长
        /// </summary>
        public bool IsIdentity { get; set; }

        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPK { get; set; }

        /// <summary>
        /// 列数据类型,数据库类型
        /// </summary>
        public string ColumnDataType { get; set; } = string.Empty;

        /// <summary>
        /// 来自.net的数据类型,返回String.String等字符串
        /// </summary>
        public string ColumnDataTypeByCSharp { get; set; }

        /// <summary>
        /// 所占字节大小
        /// </summary>
        public int ColumnByteSize { get; set; }

        /// <summary>
        /// 长度
        /// </summary>
        public int ColumnLength { get; set; }

        /// <summary>
        /// 小数位数
        /// </summary>
        public int Scale { get; set; }

        /// <summary>
        /// 是否允许空
        /// </summary>
        public bool IsNullAble { get; set; }

        /// <summary>
        /// 缺省值
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// 是否使用,和系统表做比较后如果系统表中不存在则更改IsUse=0(falses)
        /// </summary>
        public bool IsUse { get; set; }

        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }
    }
}