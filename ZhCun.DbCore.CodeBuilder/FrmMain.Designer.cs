﻿namespace ZhCun.DbCore.CodeBuilder
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDatabaseType = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel_proc = new System.Windows.Forms.Panel();
            this.btnSaveProcAllFile = new System.Windows.Forms.Button();
            this.btnSaveProcFile = new System.Windows.Forms.Button();
            this.btnRefreshProc = new System.Windows.Forms.Button();
            this.coBoxProc = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBuildProcCode = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSaveAllFile = new System.Windows.Forms.Button();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.btnBuildCode = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRefreshTable = new System.Windows.Forms.Button();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.txtNameSpace = new System.Windows.Forms.TextBox();
            this.txtConnectStr = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.coBoxTable = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.panel_proc.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDatabaseType);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.panel_proc);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.btnSaveAllFile);
            this.groupBox1.Controls.Add(this.btnSaveFile);
            this.groupBox1.Controls.Add(this.btnBuildCode);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnRefreshTable);
            this.groupBox1.Controls.Add(this.txtSavePath);
            this.groupBox1.Controls.Add(this.txtNameSpace);
            this.groupBox1.Controls.Add(this.txtConnectStr);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.coBoxTable);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(988, 152);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设置";
            // 
            // txtDatabaseType
            // 
            this.txtDatabaseType.Location = new System.Drawing.Point(836, 49);
            this.txtDatabaseType.Name = "txtDatabaseType";
            this.txtDatabaseType.Size = new System.Drawing.Size(124, 21);
            this.txtDatabaseType.TabIndex = 27;
            this.txtDatabaseType.TextChanged += new System.EventHandler(this.Config_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(759, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 26;
            this.label6.Text = "数据库类型：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(319, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 25;
            this.label3.Text = "保存路径：";
            // 
            // panel_proc
            // 
            this.panel_proc.Controls.Add(this.btnSaveProcAllFile);
            this.panel_proc.Controls.Add(this.btnSaveProcFile);
            this.panel_proc.Controls.Add(this.btnRefreshProc);
            this.panel_proc.Controls.Add(this.coBoxProc);
            this.panel_proc.Controls.Add(this.label2);
            this.panel_proc.Controls.Add(this.btnBuildProcCode);
            this.panel_proc.Location = new System.Drawing.Point(16, 105);
            this.panel_proc.Name = "panel_proc";
            this.panel_proc.Size = new System.Drawing.Size(976, 38);
            this.panel_proc.TabIndex = 24;
            // 
            // btnSaveProcAllFile
            // 
            this.btnSaveProcAllFile.Enabled = false;
            this.btnSaveProcAllFile.Location = new System.Drawing.Point(849, 8);
            this.btnSaveProcAllFile.Name = "btnSaveProcAllFile";
            this.btnSaveProcAllFile.Size = new System.Drawing.Size(95, 25);
            this.btnSaveProcAllFile.TabIndex = 24;
            this.btnSaveProcAllFile.Text = "保存所有文件";
            this.btnSaveProcAllFile.UseVisualStyleBackColor = true;
            this.btnSaveProcAllFile.Click += new System.EventHandler(this.BtnSaveProcAllFile_Click);
            // 
            // btnSaveProcFile
            // 
            this.btnSaveProcFile.Enabled = false;
            this.btnSaveProcFile.Location = new System.Drawing.Point(733, 8);
            this.btnSaveProcFile.Name = "btnSaveProcFile";
            this.btnSaveProcFile.Size = new System.Drawing.Size(95, 25);
            this.btnSaveProcFile.TabIndex = 25;
            this.btnSaveProcFile.Text = "保存文件";
            this.btnSaveProcFile.UseVisualStyleBackColor = true;
            this.btnSaveProcFile.Click += new System.EventHandler(this.BtnSaveProcFile_Click);
            // 
            // btnRefreshProc
            // 
            this.btnRefreshProc.Enabled = false;
            this.btnRefreshProc.Location = new System.Drawing.Point(553, 8);
            this.btnRefreshProc.Name = "btnRefreshProc";
            this.btnRefreshProc.Size = new System.Drawing.Size(75, 25);
            this.btnRefreshProc.TabIndex = 22;
            this.btnRefreshProc.Text = "刷新";
            this.btnRefreshProc.UseVisualStyleBackColor = true;
            this.btnRefreshProc.Click += new System.EventHandler(this.BtnRefreshProc_Click);
            // 
            // coBoxProc
            // 
            this.coBoxProc.FormattingEnabled = true;
            this.coBoxProc.Location = new System.Drawing.Point(71, 8);
            this.coBoxProc.Name = "coBoxProc";
            this.coBoxProc.Size = new System.Drawing.Size(464, 20);
            this.coBoxProc.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "存储过程:";
            // 
            // btnBuildProcCode
            // 
            this.btnBuildProcCode.Enabled = false;
            this.btnBuildProcCode.Location = new System.Drawing.Point(637, 8);
            this.btnBuildProcCode.Name = "btnBuildProcCode";
            this.btnBuildProcCode.Size = new System.Drawing.Size(75, 25);
            this.btnBuildProcCode.TabIndex = 23;
            this.btnBuildProcCode.Text = "生成代码";
            this.btnBuildProcCode.UseVisualStyleBackColor = true;
            this.btnBuildProcCode.Click += new System.EventHandler(this.BtnBuildProcCode_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(885, 15);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 26);
            this.btnConnect.TabIndex = 21;
            this.btnConnect.Text = "连接";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.BtnConnect_Click);
            // 
            // btnSaveAllFile
            // 
            this.btnSaveAllFile.Enabled = false;
            this.btnSaveAllFile.Location = new System.Drawing.Point(865, 77);
            this.btnSaveAllFile.Name = "btnSaveAllFile";
            this.btnSaveAllFile.Size = new System.Drawing.Size(95, 25);
            this.btnSaveAllFile.TabIndex = 23;
            this.btnSaveAllFile.Text = "保存所有文件";
            this.btnSaveAllFile.UseVisualStyleBackColor = true;
            this.btnSaveAllFile.Click += new System.EventHandler(this.BtnSaveAllFile_Click);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.Enabled = false;
            this.btnSaveFile.Location = new System.Drawing.Point(749, 77);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(95, 25);
            this.btnSaveFile.TabIndex = 23;
            this.btnSaveFile.Text = "保存文件";
            this.btnSaveFile.UseVisualStyleBackColor = true;
            this.btnSaveFile.Click += new System.EventHandler(this.BtnSaveFile_Click);
            // 
            // btnBuildCode
            // 
            this.btnBuildCode.Enabled = false;
            this.btnBuildCode.Location = new System.Drawing.Point(653, 77);
            this.btnBuildCode.Name = "btnBuildCode";
            this.btnBuildCode.Size = new System.Drawing.Size(75, 25);
            this.btnBuildCode.TabIndex = 23;
            this.btnBuildCode.Text = "生成代码";
            this.btnBuildCode.UseVisualStyleBackColor = true;
            this.btnBuildCode.Click += new System.EventHandler(this.BtnBuildCode_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 17;
            this.label1.Text = "命名空间:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "连接字符串:";
            // 
            // btnRefreshTable
            // 
            this.btnRefreshTable.Enabled = false;
            this.btnRefreshTable.Location = new System.Drawing.Point(568, 77);
            this.btnRefreshTable.Name = "btnRefreshTable";
            this.btnRefreshTable.Size = new System.Drawing.Size(75, 25);
            this.btnRefreshTable.TabIndex = 22;
            this.btnRefreshTable.Text = "刷新";
            this.btnRefreshTable.UseVisualStyleBackColor = true;
            this.btnRefreshTable.Click += new System.EventHandler(this.BtnRefreshTable_Click);
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(390, 49);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.Size = new System.Drawing.Size(361, 21);
            this.txtSavePath.TabIndex = 18;
            this.txtSavePath.TextChanged += new System.EventHandler(this.Config_TextChanged);
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Location = new System.Drawing.Point(86, 49);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.Size = new System.Drawing.Size(227, 21);
            this.txtNameSpace.TabIndex = 18;
            this.txtNameSpace.TextChanged += new System.EventHandler(this.Config_TextChanged);
            // 
            // txtConnectStr
            // 
            this.txtConnectStr.Location = new System.Drawing.Point(86, 19);
            this.txtConnectStr.Name = "txtConnectStr";
            this.txtConnectStr.Size = new System.Drawing.Size(792, 21);
            this.txtConnectStr.TabIndex = 18;
            this.txtConnectStr.TextChanged += new System.EventHandler(this.Config_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 19;
            this.label4.Text = "表(视图):";
            // 
            // coBoxTable
            // 
            this.coBoxTable.FormattingEnabled = true;
            this.coBoxTable.Location = new System.Drawing.Point(86, 79);
            this.coBoxTable.Name = "coBoxTable";
            this.coBoxTable.Size = new System.Drawing.Size(464, 20);
            this.coBoxTable.TabIndex = 20;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCode);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 152);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(988, 496);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "代码";
            // 
            // txtCode
            // 
            this.txtCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode.HideSelection = false;
            this.txtCode.Location = new System.Drawing.Point(3, 17);
            this.txtCode.Multiline = true;
            this.txtCode.Name = "txtCode";
            this.txtCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCode.Size = new System.Drawing.Size(982, 476);
            this.txtCode.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 648);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZhCun.DbCore Model实体生成器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel_proc.ResumeLayout(false);
            this.panel_proc.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel_proc;
        private System.Windows.Forms.Button btnRefreshProc;
        private System.Windows.Forms.ComboBox coBoxProc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBuildProcCode;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSaveAllFile;
        private System.Windows.Forms.Button btnSaveFile;
        private System.Windows.Forms.Button btnBuildCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRefreshTable;
        private System.Windows.Forms.TextBox txtNameSpace;
        private System.Windows.Forms.TextBox txtConnectStr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox coBoxTable;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Button btnSaveProcAllFile;
        private System.Windows.Forms.Button btnSaveProcFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSavePath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDatabaseType;
    }
}

