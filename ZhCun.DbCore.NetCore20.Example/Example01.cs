﻿/**************************************************************************
创建日期:	2019/5/12 21:40:51     
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)    ZhCun.cn

描	述：增\改\删 的各方法使用例子
记	录：
***************************************************************************/
using System;
using ZhCun.DbCore.Cores;
using ZhCun.DbCore.Example.DbModels;

namespace ZhCun.DbCore.Example
{
    class Example01
    {
        //创建db上下文，注意：实际项目除非winform非异步操作，不建议使用全局静态对象；
        static DBContext01 db = new DBContext01();
        public static void AddTestData()
        {
            for (int i = 0; i < 50; i++)
            {
                TUser t = new TUser
                {
                    Id = Guid.NewGuid(),
                    UserName = "UserName" + i,
                    LoginName = "LoginName" + i,
                    LoginPwd = "LoginPwd" + i
                };
                db.Insert(t);
            }
        }
        public static void AddUser()
        {
            TUser t = new TUser
            {
                Id = Guid.NewGuid(),
                UserName = "张三",
                LoginName = "zhangsan",
                LoginPwd = "12345"
            };
            ExecResult r = db.Insert(t);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        public static void AddDeviceType()
        {
            TDeviceType t = new TDeviceType()
            {
                Id = Guid.NewGuid(),
                TypeName = "类型1",
                CreateTime = DateTime.Now
            };
            ExecResult r = db.Insert(t);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        /// <summary>
        /// 根据主键自动条件更新
        /// </summary>
        public static void Update1()
        {
            //根据主键自动条件更新
            //只修改名字
            var t = new TUser
            {
                Id = Guid.Parse("A0FCF84C-4EBD-4735-863A-205D8536DA93"),
                UserName = "修改后的张三"
            };
            ExecResult r = db.Update(t);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        public static void Update2()
        {
            //使用Lamda表达式条件更新
            var t = new TUser();
            t.UserName = "张三update2";
            var id = Guid.Parse("A0FCF84C-4EBD-4735-863A-205D8536DA93");
            var r = db.Update(t, s => s.Id == id);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        public static void Update3()
        {
            //使用Where对象批量更新
            var t = new TUser { UserName = "张三Update3" };
            var where = db.CreateWhere<TUser>();
            where.WhereAnd(s => s.UserName == "张三update2" || s.UserName == "修改后的张三");
            var r = db.Update(t, where);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        public static void Delete1()
        {
            //默认按主键条件删除
            var t = new TUser();
            t.Id = Guid.Parse("C25E106E-70A2-4518-AB31-3D0D26BA2736");
            var r = db.Delete(t);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        public static void Delete2()
        {
            //指定条件删除
            var r = db.Delete<TUser>(s => s.UserName == "张三");
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        public static void Delete3()
        {
            var where = db.CreateWhere<TUser>();
            Guid id1 = Guid.Parse("A0FCF84C-4EBD-4735-863A-205D8536DA93");
            where.WhereAnd(s => s.WEx_In(s.Id, id1));
            var r = db.Delete<TUser>(where);
            Console.WriteLine("执行完成，影响 {0} 行", r.RowCount);
        }
        //事务测试
        public static void Trans1()
        {
            try
            {
                db.TransStart();
                TUser user = new TUser();
                user.Id = Guid.NewGuid();
                user.UserName = "王五";
                user.LoginName = "wangwu";
                db.Insert(user);

                TDevice dev = new TDevice();
                dev.Id = Guid.NewGuid();
                dev.DevName = "笔记本";
                db.Insert(dev);

                db.TransCommit();
                Console.WriteLine("事务执行完成");
            }
            catch (Exception ex)
            {
                db.TransRollback();
                Console.WriteLine(ex.Message);
            }
        }
        public static void Trans2()
        {
            try
            {
                db.TransStart();
                TUser user = new TUser
                {
                    Id = Guid.NewGuid(),
                    UserName = "王五22",
                    LoginName = "wangwu"
                };
                db.Insert(user);

                TUser user2 = new TUser
                {
                    Id = Guid.NewGuid(),
                    UserName = "王五33",
                    LoginName = "wangwu"
                };
                db.Insert(user2);

                TDevice dev = new TDevice
                {
                    Id = Guid.NewGuid(),
                    DevName = "笔记本44"
                };
                db.Insert(dev);

                TDevice dev2 = new TDevice
                {
                    // Id = Guid.NewGuid(), 主键为空的异常会触发事务回滚
                    DevName = "笔记本55"
                };
                db.Insert(dev2);

                db.TransCommit();
                Console.WriteLine("事务执行完成");
            }
            catch (Exception ex)
            {
                db.TransRollback();
                Console.WriteLine("事务回滚");
                Console.WriteLine(ex.Message);
            }
        }
    }
}