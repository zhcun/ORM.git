﻿/**************************************************************************
创建日期:	2019/5/15
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)  ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ZhCun.DbCore.Example
{
    class Common
    {
        public static void PrintList<TEntity>(List<TEntity> list)
        {
            if (list == null)
            {
                Console.WriteLine("没有数据");
                return;
            }
            Type t = typeof(TEntity);
            PropertyInfo[] pInfos = t.GetProperties();
            if (pInfos != null)
            {
                foreach (var item in pInfos)
                {
                    Console.Write("{0}\t", item.Name);
                }
                Console.WriteLine();

                foreach (TEntity obj in list)
                {
                    foreach (PropertyInfo item in pInfos)
                    {
                        //string pName = item.Name;
                        object pValue = item.GetValue(obj, null);
                        Console.Write("[{0}] ", pValue);
                        //Console.Write("{0}：{1}", pName, pValue);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
