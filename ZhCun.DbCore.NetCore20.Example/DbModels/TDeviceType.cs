using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.DbCore.Example.DbModels
{
	public partial class TDeviceType : EntityBase
	{
		private System.Guid _Id;
		/// <summary>
		/// Id
		/// </summary>
		[EntityAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.Guid Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _TypeName;
		/// <summary>
		/// TypeName
		/// </summary>
		[EntityAttribute(ColumnName = CNTypeName)]
		public System.String TypeName
		{
			get { return _TypeName; }
			set
			{
				_TypeName = value;
				base.SetFieldChanged(CNTypeName) ;
			}
		}

		private System.DateTime? _CreateTime;
		/// <summary>
		/// CreateTime
		/// </summary>
		[EntityAttribute(ColumnName = CNCreateTime)]
		public System.DateTime? CreateTime
		{
			get { return _CreateTime; }
			set
			{
				_CreateTime = value;
				base.SetFieldChanged(CNCreateTime) ;
			}
		}

		private System.DateTime? _ModifyTime;
		/// <summary>
		/// ModifyTime
		/// </summary>
		[EntityAttribute(ColumnName = CNModifyTime)]
		public System.DateTime? ModifyTime
		{
			get { return _ModifyTime; }
			set
			{
				_ModifyTime = value;
				base.SetFieldChanged(CNModifyTime) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNTypeName = "TypeName";
		public const string CNCreateTime = "CreateTime";
		public const string CNModifyTime = "ModifyTime";
		#endregion

	}
}
