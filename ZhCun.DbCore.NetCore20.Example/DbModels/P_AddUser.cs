using System.Data;
using ZhCun.DbCore.Entitys;

namespace ZhCun.DbCore.Example.DbModels
{
	public class P_AddUser : ProcEntityBase
	{
		public string LoginName { set; get; }

		public string LoginPwd { set; get; }
        
		public string UserName { set; get; }

        [ProcParam(ParamDirection = ParameterDirection.Output)]
        public string Message { set; get; }

        [ProcParam(ParamDirection = ParameterDirection.Output)]
        public bool Success { set; get; }
    }
}
