﻿/*
	功能: 增加一组测试数据
	描述:				
			
	作者: 张存
	日期: 2019.5.15
	修改记录(注明修改人,修改时间):
		
		
*/

ALTER Proc [dbo].[P_AddTestData]
as
	insert into TUser (Id,LoginName,LoginPwd,UserName,Birthday,Email,CreateTime)
	select NEWID(),'zhangsan','zs123','张三','1980-01-01','zhangsan@zhcun.cn',GETDATE() union all
	select NEWID(),'lisi','ls123','李四','1990-01-01','lisi@zhcun.cn',GETDATE() union all
	select NEWID(),'wangwu','ww123','王五','1989-08-08','wangwu@zhcun.cn',GETDATE() union all
	select NEWID(),'maliu','ml123','马六','1991-03-08','maliu@zhcun.cn',GETDATE() union all
	select NEWID(),'zhaoqi','zq123','赵七','1995-07-08','zhaoqi@zhcun.cn',GETDATE() union all
	select NEWID(),'liuba','lb123','刘八','1987-08-08','liuba@zhcun.cn',GETDATE() union all
	select NEWID(),'chenjiu','cj123','陈九','1988-01-08','chenjiu@zhcun.cn',GETDATE() union all
	select NEWID(),'sunshi','sh123','孙十','1988-08-08','sunshi@zhcun.cn',GETDATE()