﻿/*
	功能: 删除所有数据
	描述:				
			
	作者: 张存
	日期: 2019.5.15
	修改记录(注明修改人,修改时间):
		
		
*/
Create Proc P_DelAll
as
delete from TUser
delete from TDevice
delete from TDeviceType