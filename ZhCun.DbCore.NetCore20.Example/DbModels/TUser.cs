using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.DbCore.Example.DbModels
{
	public partial class TUser : EntityBase
	{
		private System.Guid _Id;
		/// <summary>
		/// Id
		/// </summary>
		[EntityAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.Guid Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _LoginName;
		/// <summary>
		/// LoginName
		/// </summary>
		[EntityAttribute(ColumnName = CNLoginName)]
		public System.String LoginName
		{
			get { return _LoginName; }
			set
			{
				_LoginName = value;
				base.SetFieldChanged(CNLoginName) ;
			}
		}

		private System.String _LoginPwd;
		/// <summary>
		/// LoginPwd
		/// </summary>
		[EntityAttribute(ColumnName = CNLoginPwd)]
		public System.String LoginPwd
		{
			get { return _LoginPwd; }
			set
			{
				_LoginPwd = value;
				base.SetFieldChanged(CNLoginPwd) ;
			}
		}

		private System.String _UserName;
		/// <summary>
		/// UserName
		/// </summary>
		[EntityAttribute(ColumnName = CNUserName)]
		public System.String UserName
		{
			get { return _UserName; }
			set
			{
				_UserName = value;
				base.SetFieldChanged(CNUserName) ;
			}
		}

		private System.DateTime? _Birthday;
		/// <summary>
		/// Birthday
		/// </summary>
		[EntityAttribute(ColumnName = CNBirthday)]
		public System.DateTime? Birthday
		{
			get { return _Birthday; }
			set
			{
				_Birthday = value;
				base.SetFieldChanged(CNBirthday) ;
			}
		}

		private System.String _Email;
		/// <summary>
		/// Email
		/// </summary>
		[EntityAttribute(ColumnName = CNEmail)]
		public System.String Email
		{
			get { return _Email; }
			set
			{
				_Email = value;
				base.SetFieldChanged(CNEmail) ;
			}
		}

		private System.DateTime? _CreateTime;
		/// <summary>
		/// CreateTime
		/// </summary>
		[EntityAttribute(ColumnName = CNCreateTime)]
		public System.DateTime? CreateTime
		{
			get { return _CreateTime; }
			set
			{
				_CreateTime = value;
				base.SetFieldChanged(CNCreateTime) ;
			}
		}

		private System.DateTime? _ModifyTime;
		/// <summary>
		/// ModifyTime
		/// </summary>
		[Entity(ColumnName = CNModifyTime)]
		public System.DateTime? ModifyTime
		{
			get { return _ModifyTime; }
			set
			{
				_ModifyTime = value;
				base.SetFieldChanged(CNModifyTime) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNLoginName = "LoginName";
		public const string CNLoginPwd = "LoginPwd";
		public const string CNUserName = "UserName";
		public const string CNBirthday = "Birthday";
		public const string CNEmail = "Email";
		public const string CNCreateTime = "CreateTime";
		public const string CNModifyTime = "ModifyTime";
		#endregion

	}
}
