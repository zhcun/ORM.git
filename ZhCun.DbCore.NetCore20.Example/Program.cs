﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace ZhCun.DbCore.Example.NetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection() //将配置文件的数据加载到内存中
                .SetBasePath(Directory.GetCurrentDirectory()) //指定配置文件所在的目录
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true) //指定加载的配置文件
                .Build(); //编译成对象  

            var r = configuration["ServerCode"];
            Console.WriteLine($"ServerCode:{configuration["ServerCode"]}");

            //UserInfo user1 = new UserInfo();
            //UserInfo user2 = new UserInfo();
            //configuration.GetSection("section0").
            //configuration.GetSection("section1").Bind(user2);

            //Console.WriteLine(user1.ToString());
            //Console.WriteLine(user2.ToString());
            //Console.WriteLine($"section0:UserId:{configuration["section0:UserId"]}");


            //Example01.AddTestData();
            //Example01.AddUser();
            //Example01.AddDeviceType();
            //Example01.Update2();
            //Example01.Update3();
            //Example01.Delete1();
            //Example01.Delete2();
            //Example01.Delete3();
            //Example01.Trans1();
            //Example01.Trans2();
            //Example02.Query4();
            Example03.Proc3();
            Console.WriteLine("over");
            Console.ReadKey();
        }
    }
}
