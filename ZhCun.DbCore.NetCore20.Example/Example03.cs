﻿/**************************************************************************
创建日期:	2019/5/15
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com
创建时间:	
Copyright (c)  ZhCun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.DbCore.Example.DbModels;

namespace ZhCun.DbCore.Example
{
    class Example03
    {
        public static void Proc1()
        {
            P_AddUser p = new P_AddUser();
            p.LoginName = "张存";
            p.LoginPwd = "112233";
            p.UserName = "";
            var db = new DBContext01();
            var r = db.ExecProcedure(p);
            r.ExecProc();
        }

        public static void Proc2()
        {
            P_AddTestData p = new P_AddTestData();
            var db = new DBContext01();
            var r = db.ExecProcedure(p);
            r.ExecProc();
        }

        public static void Proc3()
        {
            P_GetUser p = new P_GetUser();
            var db = new DBContext01();
            var r = db.ExecProcedure(p);
            var rList = r.ToList<TUser>();
            Common.PrintList(rList);
        }
    }
}