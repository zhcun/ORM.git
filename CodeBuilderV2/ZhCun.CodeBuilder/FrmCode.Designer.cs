﻿
namespace ZhCun.CodeBuilder
{
    partial class FrmCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv = new ZhCun.Win.Controls.GridView();
            this.Caption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ucPageNav1 = new ZhCun.Win.Controls.UcPageNav();
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.tsTxtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tsBtnSearch = new System.Windows.Forms.ToolStripButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.grpCode = new System.Windows.Forms.GroupBox();
            this.dcm = new ZhCun.Win.Extend.DCM(this.components);
            this.txtDBType = new ZhCun.Win.Controls.EditComboBox();
            this.txtSavePath = new ZhCun.Win.Controls.EditText();
            this.editText3 = new ZhCun.Win.Controls.EditText();
            this.txtNameSpace = new ZhCun.Win.Controls.EditText();
            this.editText1 = new ZhCun.Win.Controls.EditText();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtTableSearch = new ZhCun.Win.Controls.EditText();
            this.lbTable = new System.Windows.Forms.ListBox();
            this.lbProc = new System.Windows.Forms.ListBox();
            this.txtProcSearch = new ZhCun.Win.Controls.EditText();
            this.cMenuTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsMenuSaveALL = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tsBtnSaveCurr = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveToFile = new System.Windows.Forms.Button();
            this.btnSetClipBoard = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tsMenuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.toolStripTop.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpCode.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.cMenuTable.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv);
            this.groupBox1.Controls.Add(this.toolStripTop);
            this.groupBox1.Controls.Add(this.ucPageNav1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(291, 677);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "连接配置";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.CheckBoxCellClick = false;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Caption});
            this.dgv.DisplayRowNo = false;
            this.dgv.DisplayRowNoStyle = ZhCun.Win.Controls.GridView.RowNoStyle.CustomColumn;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(3, 55);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(285, 577);
            this.dgv.TabIndex = 38;
            this.dgv.UseControlStyle = true;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // Caption
            // 
            this.Caption.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Caption.DataPropertyName = "Caption";
            this.Caption.HeaderText = "标题";
            this.Caption.Name = "Caption";
            this.Caption.ReadOnly = true;
            // 
            // ucPageNav1
            // 
            this.ucPageNav1.AutoSize = true;
            this.ucPageNav1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPageNav1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucPageNav1.IsMini = true;
            this.ucPageNav1.Location = new System.Drawing.Point(3, 632);
            this.ucPageNav1.Name = "ucPageNav1";
            this.ucPageNav1.Size = new System.Drawing.Size(285, 42);
            this.ucPageNav1.TabIndex = 39;
            // 
            // toolStripTop
            // 
            this.toolStripTop.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTxtSearch,
            this.tsBtnSearch});
            this.toolStripTop.Location = new System.Drawing.Point(3, 22);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(285, 33);
            this.toolStripTop.TabIndex = 40;
            this.toolStripTop.Text = "工具栏";
            // 
            // tsTxtSearch
            // 
            this.tsTxtSearch.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsTxtSearch.Margin = new System.Windows.Forms.Padding(5);
            this.tsTxtSearch.Name = "tsTxtSearch";
            this.tsTxtSearch.Size = new System.Drawing.Size(150, 23);
            this.tsTxtSearch.ToolTipText = "输入回车进行快速检索";
            // 
            // tsBtnSearch
            // 
            this.tsBtnSearch.Checked = true;
            this.tsBtnSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnSearch.Name = "tsBtnSearch";
            this.tsBtnSearch.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnSearch.Size = new System.Drawing.Size(61, 30);
            this.tsBtnSearch.Text = "检索";
            this.tsBtnSearch.Click += new System.EventHandler(this.tsBtnSearch_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(291, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 677);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnConnect);
            this.groupBox2.Controls.Add(this.txtDBType);
            this.groupBox2.Controls.Add(this.txtSavePath);
            this.groupBox2.Controls.Add(this.editText3);
            this.groupBox2.Controls.Add(this.txtNameSpace);
            this.groupBox2.Controls.Add(this.editText1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(294, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(952, 127);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "配置详情";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(294, 127);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(271, 550);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "对象列表";
            // 
            // grpCode
            // 
            this.grpCode.Controls.Add(this.txtCode);
            this.grpCode.Controls.Add(this.panel1);
            this.grpCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCode.Location = new System.Drawing.Point(565, 127);
            this.grpCode.Name = "grpCode";
            this.grpCode.Size = new System.Drawing.Size(681, 550);
            this.grpCode.TabIndex = 4;
            this.grpCode.TabStop = false;
            this.grpCode.Text = "代码";
            // 
            // txtDBType
            // 
            this.txtDBType.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtDBType, "DBType");
            this.dcm.SetEnterIsTab(this.txtDBType, true);
            this.txtDBType.FormattingEnabled = true;
            this.dcm.SetIsUse(this.txtDBType, true);
            this.txtDBType.Items.AddRange(new object[] {
            "SqlServer",
            "SQLite",
            "MySql",
            "Oracle"});
            this.txtDBType.Location = new System.Drawing.Point(388, 18);
            this.txtDBType.Name = "txtDBType";
            this.txtDBType.Size = new System.Drawing.Size(140, 28);
            this.txtDBType.TabIndex = 4;
            this.dcm.SetTabIndex(this.txtDBType, 0);
            this.txtDBType.ValueItem = -1;
            this.dcm.SetValueMember(this.txtDBType, "");
            // 
            // txtSavePath
            // 
            this.txtSavePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSavePath.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtSavePath, "SavePath");
            this.txtSavePath.EmptyTextTip = null;
            this.txtSavePath.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtSavePath, true);
            this.dcm.SetIsUse(this.txtSavePath, true);
            this.txtSavePath.Location = new System.Drawing.Point(125, 54);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.RealValue = "";
            this.txtSavePath.Size = new System.Drawing.Size(810, 26);
            this.txtSavePath.TabIndex = 5;
            this.dcm.SetTabIndex(this.txtSavePath, 0);
            this.txtSavePath.ValueItem = null;
            this.dcm.SetValueMember(this.txtSavePath, "");
            // 
            // editText3
            // 
            this.editText3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText3.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText3, "ConnString");
            this.editText3.EmptyTextTip = null;
            this.editText3.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText3, true);
            this.dcm.SetIsUse(this.editText3, true);
            this.editText3.Location = new System.Drawing.Point(125, 86);
            this.editText3.Name = "editText3";
            this.editText3.RealValue = "";
            this.editText3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.editText3.Size = new System.Drawing.Size(810, 26);
            this.editText3.TabIndex = 6;
            this.dcm.SetTabIndex(this.editText3, 0);
            this.editText3.ValueItem = null;
            this.dcm.SetValueMember(this.editText3, "");
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtNameSpace, "NameSpace");
            this.txtNameSpace.EmptyTextTip = null;
            this.txtNameSpace.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtNameSpace, true);
            this.dcm.SetIsUse(this.txtNameSpace, true);
            this.txtNameSpace.Location = new System.Drawing.Point(610, 19);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.RealValue = "";
            this.txtNameSpace.Size = new System.Drawing.Size(218, 26);
            this.txtNameSpace.TabIndex = 7;
            this.dcm.SetTabIndex(this.txtNameSpace, 0);
            this.txtNameSpace.ValueItem = null;
            this.dcm.SetValueMember(this.txtNameSpace, "");
            // 
            // editText1
            // 
            this.editText1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText1, "Caption");
            this.editText1.EmptyTextTip = null;
            this.editText1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText1, true);
            this.dcm.SetIsUse(this.editText1, true);
            this.editText1.Location = new System.Drawing.Point(125, 20);
            this.editText1.Name = "editText1";
            this.editText1.ReadOnly = true;
            this.editText1.RealValue = "";
            this.editText1.Size = new System.Drawing.Size(168, 26);
            this.editText1.TabIndex = 8;
            this.dcm.SetTabIndex(this.editText1, 0);
            this.editText1.ValueItem = null;
            this.dcm.SetValueMember(this.editText1, "Id");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "连接字符串：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "保存路径：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(534, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "命名空间：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(299, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "数据库类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "标题：";
            // 
            // txtCode
            // 
            this.txtCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode.Location = new System.Drawing.Point(3, 74);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(675, 473);
            this.txtCode.TabIndex = 0;
            this.txtCode.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 22);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(265, 525);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lbTable);
            this.tabPage1.Controls.Add(this.txtTableSearch);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(257, 492);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "表&视图";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lbProc);
            this.tabPage2.Controls.Add(this.txtProcSearch);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(257, 492);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "存储过程";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtTableSearch
            // 
            this.txtTableSearch.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtTableSearch, "");
            this.txtTableSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTableSearch.EmptyTextTip = null;
            this.txtTableSearch.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtTableSearch, true);
            this.dcm.SetIsUse(this.txtTableSearch, true);
            this.txtTableSearch.Location = new System.Drawing.Point(3, 3);
            this.txtTableSearch.Name = "txtTableSearch";
            this.txtTableSearch.RealValue = "";
            this.txtTableSearch.Size = new System.Drawing.Size(251, 26);
            this.txtTableSearch.TabIndex = 0;
            this.dcm.SetTabIndex(this.txtTableSearch, 0);
            this.txtTableSearch.ValueItem = null;
            this.dcm.SetValueMember(this.txtTableSearch, "");
            this.txtTableSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTableSearch_KeyDown);
            // 
            // lbTable
            // 
            this.lbTable.ContextMenuStrip = this.cMenuTable;
            this.lbTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTable.FormattingEnabled = true;
            this.lbTable.ItemHeight = 20;
            this.lbTable.Location = new System.Drawing.Point(3, 29);
            this.lbTable.Name = "lbTable";
            this.lbTable.Size = new System.Drawing.Size(251, 460);
            this.lbTable.TabIndex = 1;
            this.lbTable.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbTable_MouseDoubleClick);
            // 
            // lbProc
            // 
            this.lbProc.ContextMenuStrip = this.cMenuTable;
            this.lbProc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbProc.FormattingEnabled = true;
            this.lbProc.ItemHeight = 20;
            this.lbProc.Location = new System.Drawing.Point(3, 29);
            this.lbProc.Name = "lbProc";
            this.lbProc.Size = new System.Drawing.Size(251, 460);
            this.lbProc.TabIndex = 3;
            this.lbProc.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbProc_MouseDoubleClick);
            // 
            // txtProcSearch
            // 
            this.txtProcSearch.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtProcSearch, "");
            this.txtProcSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtProcSearch.EmptyTextTip = null;
            this.txtProcSearch.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtProcSearch, true);
            this.dcm.SetIsUse(this.txtProcSearch, true);
            this.txtProcSearch.Location = new System.Drawing.Point(3, 3);
            this.txtProcSearch.Name = "txtProcSearch";
            this.txtProcSearch.RealValue = "";
            this.txtProcSearch.Size = new System.Drawing.Size(251, 26);
            this.txtProcSearch.TabIndex = 2;
            this.dcm.SetTabIndex(this.txtProcSearch, 0);
            this.txtProcSearch.ValueItem = null;
            this.dcm.SetValueMember(this.txtProcSearch, "");
            this.txtProcSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProcSearch_KeyDown);
            // 
            // cMenuTable
            // 
            this.cMenuTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuRefresh,
            this.tsBtnSaveCurr,
            this.tsMenuSaveALL});
            this.cMenuTable.Name = "cMenuTable";
            this.cMenuTable.Size = new System.Drawing.Size(149, 70);
            // 
            // tsMenuSaveALL
            // 
            this.tsMenuSaveALL.Name = "tsMenuSaveALL";
            this.tsMenuSaveALL.Size = new System.Drawing.Size(148, 22);
            this.tsMenuSaveALL.Text = "保存全部代码";
            this.tsMenuSaveALL.Click += new System.EventHandler(this.tsMenuSaveALL_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnSetClipBoard);
            this.panel1.Controls.Add(this.btnSaveToFile);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 52);
            this.panel1.TabIndex = 1;
            // 
            // tsBtnSaveCurr
            // 
            this.tsBtnSaveCurr.Name = "tsBtnSaveCurr";
            this.tsBtnSaveCurr.Size = new System.Drawing.Size(148, 22);
            this.tsBtnSaveCurr.Text = "保存选中";
            this.tsBtnSaveCurr.Click += new System.EventHandler(this.tsBtnSaveCurr_Click);
            // 
            // btnSaveToFile
            // 
            this.btnSaveToFile.Location = new System.Drawing.Point(7, 9);
            this.btnSaveToFile.Name = "btnSaveToFile";
            this.btnSaveToFile.Size = new System.Drawing.Size(75, 36);
            this.btnSaveToFile.TabIndex = 0;
            this.btnSaveToFile.Text = "保存";
            this.btnSaveToFile.UseVisualStyleBackColor = true;
            this.btnSaveToFile.Click += new System.EventHandler(this.btnSaveToFile_Click);
            // 
            // btnSetClipBoard
            // 
            this.btnSetClipBoard.Location = new System.Drawing.Point(88, 9);
            this.btnSetClipBoard.Name = "btnSetClipBoard";
            this.btnSetClipBoard.Size = new System.Drawing.Size(75, 36);
            this.btnSetClipBoard.TabIndex = 0;
            this.btnSetClipBoard.Text = "剪切板";
            this.btnSetClipBoard.UseVisualStyleBackColor = true;
            this.btnSetClipBoard.Click += new System.EventHandler(this.btnSetClipBoard_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(832, 17);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(103, 30);
            this.btnConnect.TabIndex = 14;
            this.btnConnect.Text = "重新连接";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(169, 9);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 36);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "刷新";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // tsMenuRefresh
            // 
            this.tsMenuRefresh.Name = "tsMenuRefresh";
            this.tsMenuRefresh.Size = new System.Drawing.Size(148, 22);
            this.tsMenuRefresh.Text = "刷新";
            this.tsMenuRefresh.Click += new System.EventHandler(this.tsMenuRefresh_Click);
            // 
            // FrmCode
            // 
            this.AutoInitGridView = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 677);
            this.Controls.Add(this.grpCode);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmCode";
            this.Text = "代码生成";
            this.Load += new System.EventHandler(this.FrmCode_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.grpCode.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.cMenuTable.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private Win.Controls.GridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caption;
        private Win.Controls.UcPageNav ucPageNav1;
        public System.Windows.Forms.ToolStrip toolStripTop;
        public System.Windows.Forms.ToolStripTextBox tsTxtSearch;
        public System.Windows.Forms.ToolStripButton tsBtnSearch;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox grpCode;
        private Win.Extend.DCM dcm;
        private Win.Controls.EditComboBox txtDBType;
        private Win.Controls.EditText txtSavePath;
        private Win.Controls.EditText editText3;
        private Win.Controls.EditText txtNameSpace;
        private Win.Controls.EditText editText1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtCode;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private Win.Controls.EditText txtTableSearch;
        private System.Windows.Forms.ListBox lbTable;
        private System.Windows.Forms.ListBox lbProc;
        private Win.Controls.EditText txtProcSearch;
        private System.Windows.Forms.ContextMenuStrip cMenuTable;
        private System.Windows.Forms.ToolStripMenuItem tsMenuSaveALL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem tsBtnSaveCurr;
        private System.Windows.Forms.Button btnSaveToFile;
        private System.Windows.Forms.Button btnSetClipBoard;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ToolStripMenuItem tsMenuRefresh;
    }
}