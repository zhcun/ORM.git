﻿
namespace ZhCun.CodeBuilder
{
    partial class FrmCodeConfigEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.editText1 = new ZhCun.Win.Controls.EditText();
            this.label2 = new System.Windows.Forms.Label();
            this.editComboBox1 = new ZhCun.Win.Controls.EditComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.editText2 = new ZhCun.Win.Controls.EditText();
            this.label4 = new System.Windows.Forms.Label();
            this.editText3 = new ZhCun.Win.Controls.EditText();
            this.editText4 = new ZhCun.Win.Controls.EditText();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 263);
            this.panel1.Size = new System.Drawing.Size(592, 42);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(416, 0);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(502, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "标题：";
            // 
            // editText1
            // 
            this.editText1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText1, "Caption");
            this.editText1.EmptyTextTip = null;
            this.editText1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText1, true);
            this.dcm.SetIsUse(this.editText1, true);
            this.editText1.Location = new System.Drawing.Point(112, 22);
            this.editText1.Name = "editText1";
            this.editText1.RealValue = "";
            this.editText1.Size = new System.Drawing.Size(172, 26);
            this.editText1.TabIndex = 0;
            this.dcm.SetTabIndex(this.editText1, 0);
            this.editText1.ValueItem = null;
            this.dcm.SetValueMember(this.editText1, "Id");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(299, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "数据库类型：";
            // 
            // editComboBox1
            // 
            this.editComboBox1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editComboBox1, "DBType");
            this.dcm.SetEnterIsTab(this.editComboBox1, true);
            this.editComboBox1.FormattingEnabled = true;
            this.dcm.SetIsUse(this.editComboBox1, true);
            this.editComboBox1.Items.AddRange(new object[] {
            "SqlServer",
            "SQLite",
            "MySql",
            "Oracle"});
            this.editComboBox1.Location = new System.Drawing.Point(395, 20);
            this.editComboBox1.Name = "editComboBox1";
            this.editComboBox1.Size = new System.Drawing.Size(172, 28);
            this.editComboBox1.TabIndex = 0;
            this.dcm.SetTabIndex(this.editComboBox1, 0);
            this.editComboBox1.ValueItem = -1;
            this.dcm.SetValueMember(this.editComboBox1, "");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "命名空间：";
            // 
            // editText2
            // 
            this.editText2.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText2, "NameSpace");
            this.editText2.EmptyTextTip = null;
            this.editText2.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText2, true);
            this.dcm.SetIsUse(this.editText2, true);
            this.editText2.Location = new System.Drawing.Point(112, 57);
            this.editText2.Name = "editText2";
            this.editText2.RealValue = "";
            this.editText2.Size = new System.Drawing.Size(455, 26);
            this.editText2.TabIndex = 0;
            this.dcm.SetTabIndex(this.editText2, 0);
            this.editText2.ValueItem = null;
            this.dcm.SetValueMember(this.editText2, "");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "连接字符串：";
            // 
            // editText3
            // 
            this.editText3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText3.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText3, "ConnString");
            this.editText3.EmptyTextTip = null;
            this.editText3.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText3, true);
            this.dcm.SetIsUse(this.editText3, true);
            this.editText3.Location = new System.Drawing.Point(112, 128);
            this.editText3.Multiline = true;
            this.editText3.Name = "editText3";
            this.editText3.RealValue = "";
            this.editText3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.editText3.Size = new System.Drawing.Size(455, 117);
            this.editText3.TabIndex = 0;
            this.dcm.SetTabIndex(this.editText3, 0);
            this.editText3.ValueItem = null;
            this.dcm.SetValueMember(this.editText3, "");
            // 
            // editText4
            // 
            this.editText4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText4.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText4, "SavePath");
            this.editText4.EmptyTextTip = null;
            this.editText4.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText4, true);
            this.dcm.SetIsUse(this.editText4, true);
            this.editText4.Location = new System.Drawing.Point(112, 92);
            this.editText4.Name = "editText4";
            this.editText4.RealValue = "";
            this.editText4.Size = new System.Drawing.Size(455, 26);
            this.editText4.TabIndex = 0;
            this.dcm.SetTabIndex(this.editText4, 0);
            this.editText4.ValueItem = null;
            this.dcm.SetValueMember(this.editText4, "");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "保存路径：";
            // 
            // FrmCodeConfigEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 305);
            this.Controls.Add(this.editComboBox1);
            this.Controls.Add(this.editText4);
            this.Controls.Add(this.editText3);
            this.Controls.Add(this.editText2);
            this.Controls.Add(this.editText1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmCodeConfigEdit";
            this.ShowInTaskbar = false;
            this.Text = "代码配置";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.editText1, 0);
            this.Controls.SetChildIndex(this.editText2, 0);
            this.Controls.SetChildIndex(this.editText3, 0);
            this.Controls.SetChildIndex(this.editText4, 0);
            this.Controls.SetChildIndex(this.editComboBox1, 0);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Win.Controls.EditText editText1;
        private System.Windows.Forms.Label label2;
        private Win.Controls.EditComboBox editComboBox1;
        private System.Windows.Forms.Label label3;
        private Win.Controls.EditText editText2;
        private System.Windows.Forms.Label label4;
        private Win.Controls.EditText editText3;
        private Win.Controls.EditText editText4;
        private System.Windows.Forms.Label label5;
    }
}