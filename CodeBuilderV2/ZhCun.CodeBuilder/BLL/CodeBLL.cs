﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.CodeBuilder.DBModel;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.CodeBuilder.BLL
{
    class CodeBLL : BaseBLL
    {
        public ApiResultList<TCodeConfig> GetList(ArgPageData arg)
        {
            return
            GetPagerList<TCodeConfig>(arg, (query) =>
            {
                if (arg.SearchVal.IsNotEmpty())
                {
                    var likeStr = arg.SearchValLike;
                    query.WhereAnd(s => s.WEx_Like(s.Caption, likeStr));
                }
            });
        }
    }
}
