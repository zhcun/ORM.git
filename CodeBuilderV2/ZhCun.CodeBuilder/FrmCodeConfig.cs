﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.CodeBuilder.BLL;
using ZhCun.CodeBuilder.DBModel;
using ZhCun.Utils;
using ZhCun.Win;

namespace ZhCun.CodeBuilder
{
    public partial class FrmCodeConfig : FrmBase
    {
        public FrmCodeConfig()
        {
            InitializeComponent();
        }

        CodeConfigBLL BLLObj { get; } = new CodeConfigBLL();

        ArgPageData SearchArg { get; } = new ArgPageData();

        void RefreshData()
        {
            SetPageData(1, 50, out int total);
            ucPageNav1.InitiControl(total);
        }

        private void FrmCodeConfig_Load(object sender, EventArgs e)
        {
            ucPageNav1.PageChangedEvent += SetPageData;
            RefreshData();
            dgv.CellDoubleClick += Dgv_CellDoubleClick;
        }

        private void Dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            tsBtnCopyAdd_Click(sender, e);
        }

        private void SetPageData(int pageNo, int pageSize, out int total)
        {
            SearchArg.PageNo = pageNo;
            SearchArg.PageSize = pageSize;
            SearchArg.SearchVal = tsTxtSearch.Text;
            var ret = BLLObj.GetList(SearchArg);
            dgv.DataSource = ret.data;
            total = ret.count;
        }

        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsBtnAdd_Click(object sender, EventArgs e)
        {
            FrmCodeConfigEdit frm = new FrmCodeConfigEdit();
            var saveModel = new TCodeConfig();
            var ret =
            frm.ShowDialog(new EditFormArgs
            {
                SaveHandle = (m) => BLLObj.Add(m as TCodeConfig),
                SaveModel = saveModel
            });
            if (ret)
            {
                saveModel.Id = ret.data;
                dgv.AddDataSource(saveModel);
                ShowMessage(ret.msg);
            }
        }

        private void tsBtnCopyAdd_Click(object sender, EventArgs e)
        {
            var selModel = dgv.GetSelectedClassData<TCodeConfig>();
            if (selModel == null)
            {
                ShowMessage("未选中任何记录");
                return;
            }
            FrmCodeConfigEdit frm = new FrmCodeConfigEdit();
            var ret =
            frm.ShowDialog(new EditFormArgs
            {
                SaveHandle = (m) => BLLObj.Add(m as TCodeConfig),
                SetModel = selModel,
                ClearId = true
            });
            if (ret)
            {
                selModel.Id = ret.data;
                dgv.AddDataSource(selModel);
                ShowMessage(ret.msg);
            }
        }

        private void tsBtnEdit_Click(object sender, EventArgs e)
        {
            var selModel = dgv.GetSelectedClassData<TCodeConfig>();
            if (selModel == null)
            {
                ShowMessage("未选中任何记录");
                return;
            }
            FrmCodeConfigEdit frm = new FrmCodeConfigEdit();
            var ret =
            frm.ShowDialog(new EditFormArgs
            {
                SaveHandle = (m) => BLLObj.Edit(m as TCodeConfig),
                SetModel = selModel,
            });
            if (ret)
            {
                dgv.RefreshDataSource();
                ShowMessage(ret.msg);
            }
        }

        private void tsBtnDelete_Click(object sender, EventArgs e)
        {
            var selModel = dgv.GetSelectedClassData<TCodeConfig>();
            if (selModel == null)
            {
                ShowMessage("未选中任何记录");
                return;
            }
            if (!ShowQuestion("确实要移除选中记录吗？", "删除确认")) return;

            var ret = BLLObj.Del(selModel);
            if (ret)
            {
                dgv.DelDataSource(selModel);
            }
            ShowMessage(ret.msg);
        }

        private void tsBtnSearch_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void tsTxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RefreshData();
            }
        }
    }
}