﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.Win;

namespace ZhCun.CodeBuilder
{
    public partial class FrmMain : FrmBase
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        void ShowTabForm<T>() where T : FrmBase, new()
        {
            foreach (FrmBase frm in dockPanel1.Documents)
            {
                if (frm is T)
                {
                    frm.Activate();
                    return;
                }
            }
            T t = new T();
            t.Show(dockPanel1);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            tslbLoginTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ShowQuestion("确实要退出软件吗？", "退出确认"))
            {
                e.Cancel = true;
                return;
            }
        }

        private void tsbtnConfig_Click(object sender, EventArgs e)
        {
            ShowTabForm<FrmCodeConfig>();
        }

        private void tsBtnCode_Click(object sender, EventArgs e)
        {
            ShowTabForm<FrmCode>();
        }
    }
}