﻿/*
 创建日期: 2015.8.18
    创建者:张存
    邮箱:zhangcunliang@126.com
    说明:
        数据库上下文,用于操作数据的核心类,也是用户所使用的公共实现
    修改记录: 
        2015.11.6   增加生成model代码,加 partial关键字
        2015.11.9   sqlite生成 connObj.GetSchema("Tables"); 获取不到视图,重写GetTableInfo方法追加视图的实现.
 
 */
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;

namespace ZhCun.CodeBuilder.Builders
{
    class SQLiteBuilder : BaseBuilder
    {
        public SQLiteBuilder(string connStr)
            : base(connStr)
        { }
        protected internal override System.Data.Common.DbConnection CreateConnection()
        {
            return new SQLiteConnection(base.ConnectString);
        }
        protected internal override List<ModelTable> GetTableInfo()
        {
            var rList = base.GetTableInfo();
            //源信息检测不到视图了,不知道是否与4.0有关
            DataTable viewInfo = null;
            using (DbConnection connObj = CreateConnection())
            {
                DataTable dt = null;
                connObj.ConnectionString = ConnectString;
                connObj.Open();
                dt = connObj.GetSchema("Views");
                dt.DefaultView.Sort = "Table_Name";
                viewInfo = dt.DefaultView.ToTable();
            }
            for (int i = 0; i < viewInfo.Rows.Count; i++)
            {
                ModelTable model = new ModelTable();
                model.IsUse = true;
                model.TableName = viewInfo.Rows[i]["Table_Name"].ToString();
                model.TableRemark = model.TableName;
                model.TableType = TableTypeEnum.View.ToString();
                rList.Add(model);
            }
            return rList;
        }

        public override List<string> GetProceduresList()
        {
            return null;
        }

        protected override DbCommand CreateDbCommand()
        {
            return new SQLiteCommand();
        }
    }
}