﻿using System.Data.Common;
using System.Data.OleDb;

namespace ZhCun.CodeBuilder.Builders
{
    class OleDbBuiler : BaseBuilder
    {
        public OleDbBuiler(string connStr)
            : base(connStr)
        { }
        protected internal override System.Data.Common.DbConnection CreateConnection()
        {
            return new OleDbConnection(base.ConnectString);
        }
        protected override string FormatTableName(string tableName)
        {
            return string.Format("[{0}]", tableName);
        }
        protected override DbCommand CreateDbCommand()
        {
            return new OleDbCommand();
        }
    }
}