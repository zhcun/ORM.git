﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace ZhCun.CodeBuilder.Builders
{
    class MySqlBuilder : BaseBuilder
    {
        public MySqlBuilder(string connStr)
            : base(connStr)
        { }
        protected internal override DbConnection CreateConnection()
        {
            return new MySql.Data.MySqlClient.MySqlConnection(base.ConnectString);
        }
        protected override string FormatTableName(string tableName)
        {
            return string.Format("`{0}`", tableName);
        }
        protected override DbCommand CreateDbCommand()
        {
            return new MySql.Data.MySqlClient.MySqlCommand();
        }
        protected internal override List<ModelProcParam> GetProcParamInfo(string procName)
        {
            DataTable dt;
            using (DbConnection connObj = CreateConnection())
            {
                connObj.ConnectionString = ConnectString;
                connObj.Open();
                dt = connObj.GetSchema("Procedure Parameters"); //MySql 与SqlServer 差异
                dt.DefaultView.Sort = "Specific_Name";
                
                DataTable sortDT = dt.DefaultView.ToTable();
                List<ModelProcParam> list = new List<ModelProcParam>();
                foreach (DataRow dr in sortDT.Rows)
                {
                    string dtProcName = dr["Specific_Name"].ToString();
                    if (procName.Equals(dtProcName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        ModelProcParam model = new ModelProcParam();
                        string paramName = dr["Parameter_Name"].ToString(); //参数名
                        if (string.IsNullOrEmpty(paramName)) continue;
                        model.ParamName = paramName; //删除第一个 @ 符号  （MySql 与SqlServer 差异,没有@前缀）
                        model.ParamDataType = dr["Data_Type"].ToString();
                        model.ParamDataType4CSharp = GetCSharpDataTypeString(model.ParamDataType);

                        if (dr["CHARACTER_MAXIMUM_LENGTH"] != null && dr["CHARACTER_MAXIMUM_LENGTH"].ToString().Length > 0)
                            model.ParamLength = Convert.ToInt32(dr["CHARACTER_MAXIMUM_LENGTH"]);
                        model.ParamOutType = dr["PARAMETER_MODE"].ToString();
                        list.Add(model);
                    }
                }
                return list;
            }
        }

        protected internal override List<ModelTable> GetTableInfo()
        {
            DataTable tableInfo = null;
            DataTable viewInfo;
            using (DbConnection connObj = CreateConnection())
            {
                DataTable dt = null;
                connObj.ConnectionString = ConnectString;
                connObj.Open();
                dt = connObj.GetSchema("Tables");
                dt.DefaultView.Sort = "Table_Type,Table_Name";
                tableInfo = dt.DefaultView.ToTable();
                //视图
                viewInfo = connObj.GetSchema("Views", new string[] { "", connObj.Database });
            }
            List<ModelTable> list = new List<ModelTable>();
            for (int i = 0; i < tableInfo.Rows.Count; i++)
            {
                ModelTable model = new ModelTable();
                model.IsUse = true;
                model.TableName = tableInfo.Rows[i]["Table_Name"].ToString();
                model.TableRemark = model.TableName;
                model.TableType = TableTypeEnum.Table.ToString();
                
                list.Add(model);
            }
            for (int i = 0; i < viewInfo.Rows.Count; i++)
            {
                ModelTable model = new ModelTable();
                model.IsUse = true;
                model.TableName = viewInfo.Rows[i]["Table_Name"].ToString();
                model.TableRemark = model.TableName;
                model.TableType = TableTypeEnum.View.ToString();

                list.Add(model);
            }
            return list;
        }

        /// <summary>
        /// 获取存储过程列表(含函数)
        /// </summary>
        protected internal override List<ModelProc> GetProcInfo()
        {
            DataTable procInfo = null;
            using (DbConnection connObj = CreateConnection())
            {
                connObj.ConnectionString = ConnectString;
                connObj.Open();
                DataTable dt = null;
                dt = connObj.GetSchema("Procedures");
                dt.DefaultView.Sort = "Specific_Name";
                dt.DefaultView.RowFilter = "Routine_Schema <> 'sys'";
                procInfo = dt.DefaultView.ToTable();
            }
            List<ModelProc> list = new List<ModelProc>();
            // string colName = "Specific_Name";
            for (int i = 0; i < procInfo.Rows.Count; i++)
            {
                ModelProc model = new ModelProc();
                model.ProcName = procInfo.Rows[i]["Specific_Name"].ToString();
                model.ProcRemark = string.Empty;
                model.ProcType = procInfo.Rows[i]["Routine_Type"].ToString();
                list.Add(model);
            }
            return list;
        }
    }
}