﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.CodeBuilder
{
    /// <summary>
    /// 表类型枚举,表,视图
    /// </summary>
    public enum TableTypeEnum
    {
        /// <summary>
        /// 未知
        /// </summary>
        None = 0,
        /// <summary>
        /// 表
        /// </summary>
        Table = 1,
        /// <summary>
        /// 视图
        /// </summary>
        View = 2
    }

    public class ModelTable
    {
        /// <summary>
        /// 主键id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 数据库对象名,()
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 数据库对象说明
        /// </summary>
        public string TableRemark { get; set; }

        /// <summary>
        /// 表类型 Table,View,此属性可用于TableTypeEnum返回
        /// </summary>
        public string TableType { get; set; }
        public TableTypeEnum TableTypeEnum
        {
            get
            {
                TableTypeEnum tType = StringToEnum<TableTypeEnum>(TableType);
                return tType;
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 是否使用,和系统表做比较后如果系统表中不存在则更改IsUse=0(falses)
        /// </summary>
        public bool IsUse { get; set; } = true;

        /// <summary>
        /// 将字符串值转换为枚举,如果指定字符串不存在改枚举中则会引发异常
        /// </summary>
        /// <typeparam name="T">要转换的枚举类型</typeparam>
        /// <param name="enumStr">参数:枚举字符串</param>
        /// <returns>返回结果枚举</returns>
        public static T StringToEnum<T>(string enumStr)
        {
            string[] enumNames = Enum.GetNames(typeof(T));
            for (int i = 0; i < enumNames.Length; i++)
            {
                if (enumStr.Equals(enumNames[i], StringComparison.CurrentCultureIgnoreCase))
                {
                    return (T)Enum.Parse(typeof(T), enumStr);
                }
            }
            return (T)Enum.Parse(typeof(T), enumNames[0]);
        }
    }
}