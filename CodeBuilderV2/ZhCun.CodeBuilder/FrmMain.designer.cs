﻿
namespace ZhCun.CodeBuilder
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.tsbtnConfig = new System.Windows.Forms.ToolStripButton();
            this.tsBtnCode = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnExit = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbLoginTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbState = new System.Windows.Forms.ToolStripStatusLabel();
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.toolStripTop.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTop
            // 
            this.toolStripTop.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnConfig,
            this.tsBtnCode,
            this.toolStripSeparator1,
            this.tsbtnExit});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(1149, 39);
            this.toolStripTop.TabIndex = 36;
            this.toolStripTop.Text = "工具栏";
            // 
            // tsbtnConfig
            // 
            this.tsbtnConfig.BackColor = System.Drawing.Color.Transparent;
            this.tsbtnConfig.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnConfig.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsbtnConfig.Image = global::ZhCun.CodeBuilder.Properties.Resources.配置;
            this.tsbtnConfig.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnConfig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnConfig.Name = "tsbtnConfig";
            this.tsbtnConfig.Size = new System.Drawing.Size(101, 36);
            this.tsbtnConfig.Text = "配置管理";
            this.tsbtnConfig.Click += new System.EventHandler(this.tsbtnConfig_Click);
            // 
            // tsBtnCode
            // 
            this.tsBtnCode.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnCode.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnCode.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnCode.Image = global::ZhCun.CodeBuilder.Properties.Resources.code;
            this.tsBtnCode.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnCode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCode.Name = "tsBtnCode";
            this.tsBtnCode.Size = new System.Drawing.Size(101, 36);
            this.tsBtnCode.Text = "代码生成";
            this.tsBtnCode.Click += new System.EventHandler(this.tsBtnCode_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // tsbtnExit
            // 
            this.tsbtnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbtnExit.BackColor = System.Drawing.Color.Transparent;
            this.tsbtnExit.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnExit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsbtnExit.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnExit.Image")));
            this.tsbtnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExit.Name = "tsbtnExit";
            this.tsbtnExit.Size = new System.Drawing.Size(73, 36);
            this.tsbtnExit.Text = "退出";
            this.tsbtnExit.Click += new System.EventHandler(this.tsbtnExit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel4,
            this.tslbLoginTime,
            this.tslbState});
            this.statusStrip1.Location = new System.Drawing.Point(0, 645);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 18, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1149, 30);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "状态栏";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(68, 25);
            this.toolStripStatusLabel4.Text = "登录时间:";
            // 
            // tslbLoginTime
            // 
            this.tslbLoginTime.BackColor = System.Drawing.Color.Transparent;
            this.tslbLoginTime.ForeColor = System.Drawing.Color.Blue;
            this.tslbLoginTime.Name = "tslbLoginTime";
            this.tslbLoginTime.Size = new System.Drawing.Size(124, 25);
            this.tslbLoginTime.Text = "2021-08-26 22:16";
            // 
            // tslbState
            // 
            this.tslbState.AutoSize = false;
            this.tslbState.BackColor = System.Drawing.Color.Transparent;
            this.tslbState.ForeColor = System.Drawing.Color.Red;
            this.tslbState.Name = "tslbState";
            this.tslbState.Size = new System.Drawing.Size(938, 25);
            this.tslbState.Spring = true;
            this.tslbState.Text = "作者：张存     zhangcunliang@126.com";
            this.tslbState.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // dockPanel1
            // 
            this.dockPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1.Location = new System.Drawing.Point(0, 39);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Size = new System.Drawing.Size(1149, 606);
            this.dockPanel1.TabIndex = 39;
            // 
            // FrmMain
            // 
            this.AutoInitGridView = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 675);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStripTop);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IsMdiContainer = true;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "代码生成器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop;
        public System.Windows.Forms.ToolStripButton tsBtnCode;
        protected System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripButton tsbtnExit;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel tslbLoginTime;
        private System.Windows.Forms.ToolStripStatusLabel tslbState;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        public System.Windows.Forms.ToolStripButton tsbtnConfig;
    }
}

