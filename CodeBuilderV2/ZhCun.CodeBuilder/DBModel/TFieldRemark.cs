/*CodeBuilder v1.0.4 by 2022/4/11 17:14:53 */
using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.CodeBuilder.DBModel
{
	public partial class TFieldRemark : EntityBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[Entity(CNId, 0, true, IsPrimaryKey = true)]
		public System.String Id
		{
			get { return _Id; }
			set
			{
				if (!OnPropertyChanged(CNId, _Id, value)) return;
				_Id = value;
				SetFieldChanged(CNId) ;
			}
		}

		private System.String _ConfigId;
		/// <summary>
		/// ConfigId
		/// </summary>
		[Entity(CNConfigId, 1)]
		public System.String ConfigId
		{
			get { return _ConfigId; }
			set
			{
				if (!OnPropertyChanged(CNConfigId, _ConfigId, value)) return;
				_ConfigId = value;
				SetFieldChanged(CNConfigId) ;
			}
		}

		private System.String _ConfigCaption;
		/// <summary>
		/// ConfigCaption
		/// </summary>
		[Entity(CNConfigCaption, 2)]
		public System.String ConfigCaption
		{
			get { return _ConfigCaption; }
			set
			{
				if (!OnPropertyChanged(CNConfigCaption, _ConfigCaption, value)) return;
				_ConfigCaption = value;
				SetFieldChanged(CNConfigCaption) ;
			}
		}

		private System.String _ObjectName;
		/// <summary>
		/// ObjectName
		/// </summary>
		[Entity(CNObjectName, 3)]
		public System.String ObjectName
		{
			get { return _ObjectName; }
			set
			{
				if (!OnPropertyChanged(CNObjectName, _ObjectName, value)) return;
				_ObjectName = value;
				SetFieldChanged(CNObjectName) ;
			}
		}

		private System.String _Caption;
		/// <summary>
		/// Caption
		/// </summary>
		[Entity(CNCaption, 4)]
		public System.String Caption
		{
			get { return _Caption; }
			set
			{
				if (!OnPropertyChanged(CNCaption, _Caption, value)) return;
				_Caption = value;
				SetFieldChanged(CNCaption) ;
			}
		}

		private System.Boolean _IsProc;
		/// <summary>
		/// IsProc
		/// </summary>
		[Entity(CNIsProc, 5, true)]
		public System.Boolean IsProc
		{
			get { return _IsProc; }
			set
			{
				if (!OnPropertyChanged(CNIsProc, _IsProc, value)) return;
				_IsProc = value;
				SetFieldChanged(CNIsProc) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNConfigId = "ConfigId";
		public const string CNConfigCaption = "ConfigCaption";
		public const string CNObjectName = "ObjectName";
		public const string CNCaption = "Caption";
		public const string CNIsProc = "IsProc";
		#endregion

	}
}
