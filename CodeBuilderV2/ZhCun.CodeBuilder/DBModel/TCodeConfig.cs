/*CodeBuilder v1.0.4 by 2022/4/11 17:14:47 */
using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.CodeBuilder.DBModel
{
    public partial class TCodeConfig : EntityBase
    {
        private System.String _Id;
        /// <summary>
        /// Id
        /// </summary>
        [Entity(CNId, 0, IsPrimaryKey = true)]
        public System.String Id
        {
            get { return _Id; }
            set
            {
                if (!OnPropertyChanged(CNId, _Id, value)) return;
                _Id = value;
                SetFieldChanged(CNId);
            }
        }

        private System.String _Caption;
        /// <summary>
        /// Caption
        /// </summary>
        [Entity(CNCaption, 1)]
        public System.String Caption
        {
            get { return _Caption; }
            set
            {
                if (!OnPropertyChanged(CNCaption, _Caption, value)) return;
                _Caption = value;
                SetFieldChanged(CNCaption);
            }
        }

        private System.String _CaptionCode;
        /// <summary>
        /// CaptionCode
        /// </summary>
        [Entity(CNCaptionCode, 2)]
        public System.String CaptionCode
        {
            get { return _CaptionCode; }
            set
            {
                if (!OnPropertyChanged(CNCaptionCode, _CaptionCode, value)) return;
                _CaptionCode = value;
                SetFieldChanged(CNCaptionCode);
            }
        }

        private System.String _DBType;
        /// <summary>
        /// DBType
        /// </summary>
        [Entity(CNDBType, 3)]
        public System.String DBType
        {
            get { return _DBType; }
            set
            {
                if (!OnPropertyChanged(CNDBType, _DBType, value)) return;
                _DBType = value;
                SetFieldChanged(CNDBType);
            }
        }

        private System.String _NameSpace;
        /// <summary>
        /// NameSpace
        /// </summary>
        [Entity(CNNameSpace, 4)]
        public System.String NameSpace
        {
            get { return _NameSpace; }
            set
            {
                if (!OnPropertyChanged(CNNameSpace, _NameSpace, value)) return;
                _NameSpace = value;
                SetFieldChanged(CNNameSpace);
            }
        }

        private System.String _ConnString;
        /// <summary>
        /// ConnString
        /// </summary>
        [Entity(CNConnString, 5)]
        public System.String ConnString
        {
            get { return _ConnString; }
            set
            {
                if (!OnPropertyChanged(CNConnString, _ConnString, value)) return;
                _ConnString = value;
                SetFieldChanged(CNConnString);
            }
        }

        private System.String _SavePath;
        /// <summary>
        /// SavePath
        /// </summary>
        [Entity(CNSavePath, 6)]
        public System.String SavePath
        {
            get { return _SavePath; }
            set
            {
                if (!OnPropertyChanged(CNSavePath, _SavePath, value)) return;
                _SavePath = value;
                SetFieldChanged(CNSavePath);
            }
        }

        private System.String _Remark;
        /// <summary>
        /// Remark
        /// </summary>
        [Entity(CNRemark, 7)]
        public System.String Remark
        {
            get { return _Remark; }
            set
            {
                if (!OnPropertyChanged(CNRemark, _Remark, value)) return;
                _Remark = value;
                SetFieldChanged(CNRemark);
            }
        }

        #region 字段名的定义
        public const string CNId = "Id";
        public const string CNCaption = "Caption";
        public const string CNCaptionCode = "CaptionCode";
        public const string CNDBType = "DBType";
        public const string CNNameSpace = "NameSpace";
        public const string CNConnString = "ConnString";
        public const string CNSavePath = "SavePath";
        public const string CNRemark = "Remark";
        #endregion

    }
}
